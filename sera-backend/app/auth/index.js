const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth20')
const FacebookStrategy = require('passport-facebook')
const User = require('../../models/users')

passport.serializeUser((user, done) => done(null, user.userId))
passport.deserializeUser(async (userId, done) => {
    const user = await User.findById(userId)
    done(null, user)
})

let {NGROK} = process.env
if (NGROK) {
    if (NGROK.endsWith('/')) NGROK = NGROK.substring(0, -1)
} else {
    NGROK = ''
}
module.exports = app => {
    app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }))
    app.get('/auth/google/callback', passport.authenticate('google'), (req, res) => res.redirect(NGROK))
    app.get('/auth/facebook', passport.authenticate('facebook', { scope: ['email'] }))
    app.get('/auth/facebook/callback', passport.authenticate('facebook'), (req, res) => res.redirect(NGROK))
}
initGoogleAuth()
initFacebookAuth()

function initGoogleAuth() {
    const {GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET} = process.env
    passport.use(new GoogleStrategy({
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        callbackURL: NGROK + '/auth/google/callback',
        proxy: true
    }, async (accessToken, refreshToken, profile, done) => {
        let user = await User.findOne({where: {googleId: profile.id}})
        if (!user) {
            user = await User.create({
                name: profile.displayName,
                email: profile.emails[0].value,
                googleId: profile.id,
                avatar: profile.photos[0].value
            })
        }
        done(null, user)
    }))
}

function initFacebookAuth() {
    const {FACEBOOK_APP_ID, FACEBOOK_APP_SECRET} = process.env

    passport.use(new FacebookStrategy({
        clientID: FACEBOOK_APP_ID,
        clientSecret: FACEBOOK_APP_SECRET,
        callbackURL: NGROK + '/auth/facebook/callback',
        profileFields: ['id', 'displayName', 'photos', 'email']
    }, async (accessToken, refreshToken, profile, done) => {
        let user = await User.findOne({where: {facebookId: profile.id}})
        if (!user) {
            user = await User.create({
                name: profile.displayName,
                email: profile.emails[0].value,
                facebookId: profile.id,
                avatar: profile.photos[0].value
            })
        }
        done(null, user)
    }))
}
