exports.API_GET_LOGOUT = function (req, res, next) {
    req.logout()
    res.redirect('/')
}

exports.API_GET_ME = function (req, res, next) {
    res.send(req.user)
}