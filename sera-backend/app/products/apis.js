const apiUtil = require('../../utils/api_utils')
const Product = require('../../models/products')
const ParameterCategory = require('../../models/parameter_categories')
const Parameter = require('../../models/parameters')
const Review = require('../../models/reviews')
const ReviewCategory = require('../../models/review_categories')
const Producer = require('../../models/producers')
const Promise = require('bluebird')
const Op = sequelize.Op

exports.API_GET_ALL = function (req, res, next) {
    global.postgresQuery('select * from products')
        .then(data => res.status(200).send(data))
}

exports.API_GET_SEARCH = function (req, res, next) {
    if (!apiUtil.validateApiParams(['q'], req.query)) {
        res.status(400).send('Bad request')
        return
    }

    const {q} = req.query
    global.postgresQuery('SELECT * from products where products.status_active = 1 AND LOWER(name) like LOWER(\'%\' || :q || \'%\')', {q})
        .then(data => {
            data.forEach(item => item.rating = 5)
            res.status(200).send(data)
        })
}

exports.API_GET_GET = async function (req, res) {
    if (!apiUtil.validateApiParams(['id'], req.query)) {
        res.status(400).send('Bad request')
        return
    }

    try {
        const product = await Product.findById(req.query.id)
        res.status(200).send(product)
    } catch (err) {
        res.status(504).send(err.message)
    }
}

exports.API_GET_PARAMETERS = async function (req, res) {
    if (!apiUtil.validateApiParams(['product_id'], req.query)) {
        res.status(400).send('Bad request')
        return
    }

    Parameter.belongsTo(ParameterCategory, {foreignKey: 'parameter_category_id'})
    const data = await Parameter.findAll({
        where: {productId: req.query.product_id},
        include: [ParameterCategory]
    })

    res.status(200).send(data || [])
}

exports.API_GET_RATING = async function (req, res) {
    if (!apiUtil.validateApiParams(['product_id'], req.query)) {
        res.status(400).send('Bad request')
        return
    }

    const data = await postgresQuery('select sources.name, avg(comments.rating) as rating from sources, comments where comments.product_id=:pid and sources.source_id = comments.source_id GROUP BY comments.source_id, sources.name;',
        {pid: req.query.product_id}
    )

    res.status(200).send(data || [])
}

exports.API_GET_REVIEW = async function (req, res) {
    if (!apiUtil.validateApiParams(['product_id'], req.query)) {
        res.status(400).send('Bad request')
        return
    }

    Review.belongsTo(ReviewCategory, {foreignKey: 'review_category_id'})
    const data = await Review.findAll({
        where: {productId: req.query.product_id},
        include: [ReviewCategory]
    })
    res.status(200).send(data || [])
}

exports.API_GET_ALL = function (req, res, next) {
    global.postgresQuery('select products.*, producers.name as producer from products, producers where products.producer_id = producers.producer_id and products.status_active = 1')
        .then(data => res.status(200).send(data))
}

exports.API_GET_GET_PRODUCT = function (req, res, next) {
    if (!apiUtil.validateApiParams(['id'], req.query)) {
        res.status(400).send('Bad request')
        return
    }
    const {id} = req.query
    global.postgresQuery('SELECT products.*, producers.name as producer, product_categories.name as product_category ' +
        'from products, producers, product_categories ' +
        'where products.producer_id = producers.producer_id and products.product_category_id = product_categories.product_category_id and products.product_id = :id', {id})
        .then(data => res.status(200).send(data[0]))
}

exports.API_GET_GET_PRODUCT_PARAMETER = function (req, res, next) {
    if (!apiUtil.validateApiParams(['id'], req.query)) {
        res.status(400).send('Bad request')
        return
    }
    const {id} = req.query
    global.postgresQuery('SELECT * FROM parameters where parameters.producer_id :id', {id})
        .then(data => res.status(200).send(data[0]))
}

exports.API_GET_GET_PRODUCT_REVIEW = function (req, res, next) {
    if (!apiUtil.validateApiParams(['id'], req.query)) {
        res.status(400).send('Bad request')
        return
    }
    const {id} = req.query
    global.postgresQuery('SELECT * FROM reviews where reviews.producer_id :id', {id})
        .then(data => res.status(200).send(data[0]))
}

exports.API_GET_SEARCH = function (req, res, next) {
    if (!apiUtil.validateApiParams(['q'], req.query)) {
        res.status(400).send('Bad request')
        return
    }

    const {q} = req.query
    global.postgresQuery('SELECT * from products where products.status_active = 1 AND LOWER(name) like LOWER(\'%\' || :q || \'%\')', {q})
        .then(data => res.status(200).send(data))
}

exports.API_GET_ALL_PRODUCERS = function (req, res, next) {
    global.postgresQuery('select * from producers;')
        .then(data => res.status(200).send(data))
}

exports.API_GET_PARAMETER_CATEGORY = function (req, res, next) {
    global.postgresQuery('select * from parameter_categories order by parameter_category_id;')
        .then(data => res.status(200).send(data))
}

exports.API_GET_REVIEW_CATEGORY = function (req, res, next) {
    global.postgresQuery('select * from review_categories order by review_category_id;')
        .then(data => res.status(200).send(data))
}

exports.API_GET_PRODUCT_CATEGORY = function (req, res, next) {
    global.postgresQuery('select * from product_categories order by product_category_id;')
        .then(data => res.status(200).send(data))
}

exports.API_POST_ADD_PRODUCT = function (req, res, next) {
    console.log('aaaaa', req.body)
    if (!apiUtil.validateApiParams(['name', 'product_category', 'producer', 'description', 'image', 'parameter', 'review'], req.body)) {
        res.status(400).send('Bad request')
        return
    }
    console.log(req.body)
    const {name, product_category, producer, description, image, parameter, review} = req.body
    let p = JSON.stringify(parameter)
    let r = JSON.stringify(review)
    global.postgresQuery('SELECT * from addProduct(:name, :product_category, :producer, :description, :image, :p, :r)', {name, product_category, producer, description, image, p, r})
        .then(data => {res.status(200).send({
            status: "success",
            message: "product created"
        })
            console.log('thanh cong')
        })
}

exports.API_POST_EDIT_PRODUCT = function (req, res, next) {
    console.log('aaaaa', req.body)
    if (!apiUtil.validateApiParams(['product_id', 'name', 'product_category', 'producer', 'description', 'image', 'parameter', 'review'], req.body)) {
        res.status(400).send('Bad request')
        return
    }
    console.log(req.body)
    const {product_id, name, product_category, producer, description, image, parameter, review} = req.body
    let p = JSON.stringify(parameter)
    let r = JSON.stringify(review)
    global.postgresQuery("SELECT * from editProduct(:name, :product_category, :producer, :description, :image, :p, :r, '', '', '', '[]', '', :product_id)", {name, product_category, producer, description, image, p, r, product_id})
        .then(data => res.status(200).send({
            status: "success",
            message: "product edited"
        }))
}

exports.API_POST_DELETE_PRODUCT = function (req, res, next) {
    if (!apiUtil.validateApiParams(['product_id'], req.body)) {
        res.status(400).send('Bad request')
        return
    }
    console.log(req.body)
    const {product_id} = req.body
    global.postgresQuery('UPDATE products SET status_active = 0 WHERE products.product_id = :product_id', {product_id})
        .then(data => res.status(200).send({
            status: "success",
            message: "product deleted"
        }))
}

exports.API_GET_TOP = async function (req, res) {
    const topProductIds = [
        100, 111, 103, 105, 109, 115
    ]

    try {
        let products = await Product.findAll({
            where: {
                productId: {
                    [Op.in]: topProductIds
                },
                statusActive: 1
            }
        })
        if (products && products.constructor === Array) {
            console.log('1')
            const ratings = await Promise.map(products, product => {
                return postgresQuery('select avg(comments.rating) as rating from sources, comments where comments.product_id=:pid and sources.source_id = comments.source_id;',
                    {pid: product.productId}
                ).then(data => {
                    if (data) return data[0].rating
                    else return 0
                })
            })

            products = products.map((product, index) => {
                product = product.toJSON()
                product.rating = ratings[index]
                return product
            })
        }

        res.status(200).send(products)
    } catch (err) {
        res.status(504).send(err.message)
    }
}

exports.API_GET_ALL_PRODUCERS = async function (req, res) {
    res.status(200).send(await Producer.findAll({attributes: ['name', 'producerId']}))
}

exports.API_GET_PRODUCTS_BY_PRODUCER = async function(req, res) {
    console.log('aaaaaa', req.query)
    if (!apiUtil.validateApiParams(['producer_id'], req.query) && !apiUtil.validateApiParams(['producer_name'], req.query)) {
        res.status(400).send('Bad request')
        return
    }

    let {producer_id: producerId, producer_name: producerName} = req.query
    if (!producerId) {
        const producer = await Producer.findOne({where: {name: producerName}})
        if (!producer) {
            producerId = -1
        } else {
            producerId = producer.producerId
        }
    }
    const products = await Product.findAll({
        where: {
            producerId: producerId,
            statusActive: 1
        }
    })

    res.status(200).send(products)
}
