const apiUtil = require('../../utils/api_utils')

exports.API_GET_NUMBER_OF_COMMENT = function (req, res, next) {
    global.postgresQuery('SELECT * FROM getNumberOfComment();')
        .then(data => res.status(200).send(data))
}

exports.API_GET_GET_PRODUCT = function (req, res, next) {
    if (!apiUtil.validateApiParams(['id'], req.query)) {
        res.status(400).send('Bad request')
        return
    }
    const {id} = req.query
    global.postgresQuery('SELECT products.*, producers.name as producer, product_categories.name as product_category ' +
        'from products, producers, product_categories ' +
        'where products.producer_id = producers.producer_id and products.product_category_id = product_categories.product_category_id and products.product_id = :id', {id})
        .then(data => res.status(200).send(data[0]))
}