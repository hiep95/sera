const apiUtil = require('../../utils/api_utils')
const Comment = require('../../models/comments')
const User = require('../../models/users')
const Source = require('../../models/sources')
const Product = require('../../models/products')

exports.API_GET_ALL = async function(req, res, next) {
    if (!apiUtil.validateApiParams(['product_id'], req.query)) {
        res.status(400).send('Bad request')
        return
    }

    const {product_id: productId} = req.query

    Comment.belongsTo(User, {foreignKey: 'user_id'})
    const comments = await Comment.findAll({where: {productId}, order: [['createdAt', 'DESC']], include: [User]}) || []

    res.send(comments)

    // res.send(await Promise.map(comments, async comment => {
    //     comment._user = await User.findById(comment.userId)
    //     return comment
    // }))
    // res.send(comments)
}

exports.API_POST_NEW = async function(req, res, next) {
    if (!req.user) {
        res.status(401).send('You must login')
        return
    }

    if (!apiUtil.validateApiParams(['product_id', 'comment_content'], req.body.params)) {
        res.status(400).send('Bad request')
        return
    }

    // Comment length
    const {comment_content: commentText} = req.body.params
    if (commentText.length > 300 || commentText.split('\n').length > 10) {
        res.status(400).send('Comment too long')
        return
    }

    // Rating value
    let rating = +req.body.params.rating
    if (!rating || rating < 0 || rating > 100) {
	rating = 0
    }


    const product = await Product.findById(req.body.params.product_id)
    if (!product) {
        res.status(401).send('Product not found')
        return
    }

    const comment = await Comment.create({
        userId: req.user.userId,
        productId: req.body.params.product_id,
        content: commentText,
        rating
    })
    res.status(200).send(comment)
}