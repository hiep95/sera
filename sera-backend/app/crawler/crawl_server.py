
# -*- coding: utf-8 -*-
from flask import Flask
import convert_template
import flask
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import json
import re

def init_driver():
	driver = webdriver.Chrome()
	driver.wait = WebDriverWait(driver, 5)
	return driver

def lookup(driver, query):
	try:
		product=[]
		is_get=1
		while (is_get):
			try:
				driver.get("http://vnreview.vn/tim-kiem#gsc.tab=0&gsc.q=đánh giá "+query)
				is_get=0
			except:
				pass
		p=driver.find_element_by_xpath("//*[@id=\"___gcse_0\"]/div/div/div/div[5]/div[2]/div[1]/div/div[2]/div[1]/div[1]/div/a")
		url=p.get_attribute('href')
		product.append(url)
		is_get_url=1
		while(is_get_url):
			try:
				driver.get(url)
				is_get_url=0
			except:
				pass
		print(url)
		source_image_e=driver.find_element_by_xpath("//*[@id=\"imagelarge2\"]/img")
		source_image=source_image_e.get_attribute("src")
		goodnbad=driver.find_element_by_class_name("goodnbad")
		product.append(goodnbad.text)
		journal=driver.find_element_by_class_name("journal-content-article")
		product.append(journal.text)
		box_comment=driver.find_elements_by_class_name("content_right")
		list_comment=[]
		for comment in box_comment:
			try:
				user=comment.find_element_by_class_name("comment_info_username").text
				cm=comment.find_element_by_class_name("comment_content").text
				list_comment.append((user,cm,"vnreview"))
			except:
				continue
		product.append(list_comment)
		detail=""
		driver.find_element_by_id("cont-2").click()
		spec=driver.find_elements_by_class_name("product-attribute-detail")
		for s in spec:
			detail=detail+s.text
		product.append(detail)
		product.append(source_image)
	except:
		print("error")
		time.sleep(3)
		return 0
	return product
def crawl_gg(driver,query):
	list_comment=[]
	driver.get("https://www.google.com.vn/search?rlz=1C1CHBD_enVN766VN766&ei=uq0kWp-zDISs8QXj2p3ACg&q=%C4%91%C3%A1nh+gi%C3%A1+{}&oq=%C4%91%C3%A1nh+gi%C3%A1+{}&gs_l=psy-ab.3..0i71k1l4.0.0.0.137928.0.0.0.0.0.0.0.0..0.0....0...1c..64.psy-ab..0.0.0....0.sCAzt3y1H64".format(query,query))
	gg=driver.find_elements_by_class_name("g")
	for g in gg:
		try:
			title=g.find_element_by_class_name("r")
			if ("Tinhte.vn" in title.text):
				href=title.find_element_by_tag_name("a").get_attribute("href")
				if("tinhte.vn" in href):
					driver.get(href)
					list_messenger=driver.find_element_by_class_name("messageList")
					list_post=list_messenger.find_elements_by_tag_name("li")
					for p in list_post:
						try:
							box=p.find_element_by_class_name("uix_message ")
							user=box.find_element_by_class_name("username").text
							a=box.find_element_by_tag_name("article").text
							text_comment=" ".join(a.split()[1:len(a.split())])
							text_comment= re.sub("@[^\s@]+"," ",text_comment)
							if(user !=""):
								list_comment.append((user,text_comment,"tinhte.vn"))
						except:
							continue
					break
		except:
			continue
	if(len(list_comment)>0):
		list_comment.remove(list_comment[0])
	return list_comment
	# if(len(list_comment)>0):
	# 	list_comment.remove(list_comment[0])
	# 	for cm in list_comment:
	# 		f.write(cm.encode("utf-8"))
	# 		f.write("\n".encode("utf-8"))
	with open("list_sp.txt","rb") as lines:
		for line in lines:
			list_sp.append(line.strip())	
app = Flask(__name__)
@app.route("/<parameter>")
def main(parameter):
	print(parameter)
	sp=parameter.split("_")[0]
	page=parameter.split("_")[1]
	data_base=[]
	driver = init_driver()

	list_comment=[]
	data={}
	data["Name"]=sp
	product=lookup(driver,sp)

	if(product==0):
		return 0
	if(page=="vnreview"):
		list_comment=product[3]
	if(page=="tinhte"):
		list_comment=crawl_gg(driver,sp)
	if(page=="all"):
		list_comment=crawl_gg(driver,sp)
		list_comment.extend(product[3])
	data["Detail"]=product[4]
	data["Goodbad"]=product[1]
	data["journal"]=product[2]
	data["source"]=product[0]
	data["comment"]=list_comment
	data["soure_image"]=product[5]
	data_base.append(data)
	driver.quit()
	newdata=convert_template.convert_data(data_base)
	return str(newdata)
if __name__ == "__main__":
	app.run()