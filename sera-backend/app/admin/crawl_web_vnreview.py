 # -*- coding: utf-8 -*-
from selenium import webdriver
import json
import time
import sys
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import json
def init_driver():
	driver = webdriver.Chrome()
	driver.wait = WebDriverWait(driver, 5)
	return driver
def lookup(driver, query):
	try:
		product=[]
		is_get=1
		while (is_get):
			try:
				driver.get("http://vnreview.vn/tim-kiem#gsc.tab=0&gsc.q=đánh giá "+query)
				is_get=0
			except:
				pass
		p=driver.find_element_by_xpath("//*[@id=\"___gcse_0\"]/div/div/div/div[5]/div[2]/div[1]/div/div[2]/div[1]/div[1]/div/a")
		url=p.get_attribute('href')
		product.append(url)
		is_get_url=1
		while(is_get_url):
			try:
				driver.get(url)
				is_get_url=0
			except:
				pass
		goodnbad=driver.find_element_by_class_name("goodnbad")
		product.append(goodnbad.text)
		journal=driver.find_element_by_class_name("journal-content-article")
		product.append(journal.text)
		comments=driver.find_elements_by_class_name("comment_content")
		list_comment=[]
		for comment in comments:
			list_comment.append(comment.text)
		product.append(list_comment)
		detail=""
		driver.find_element_by_id("cont-2").click()
		spec=driver.find_elements_by_class_name("product-attribute-detail")
		for s in spec:
			detail=detail+s.text
		product.append(detail)
	except:
		return 0
	return product

 
 
if __name__ == "__main__":
	data_base=[]
	driver = init_driver()
	print("start")
	list_sp=sys.argv[1:]
	for sp in list_sp:
		data={}
		data["Name"]=sp
		product=lookup(driver,sp)
		if(product==0):
			continue
		data["Detail"]=product[4]
		data["Goodbad"]=product[1]
		data["journal"]=product[2]
		data["source"]=product[0]
		data["comment"]=product[3]
		with open('data_temp_'+sp+'.json', 'w') as fout:
			json.dump(data, fout,indent=5)
		print('data_temp_'+sp+'.json')
	driver.quit()