const apiUtil = require('../../utils/api_utils')
var PythonShell = require('python-shell');

exports.API_GET_ALL = function (req, res, next) {
    global.postgresQuery('select * from products')
        .then(data => res.status(200).send(data))
}

exports.API_GET_SEARCH = function (req, res, next) {
    if (!apiUtil.validateApiParams(['q'], req.query)) {
        res.status(400).send('Bad request')
        return
    }

    const {q} = req.query
    global.postgresQuery('SELECT * from products where LOWER(name) like LOWER(\'%\' || :q || \'%\')', {q})
        .then(data => res.status(200).send(data))
}

exports.API_POST_CRAWL = function (req, res, next) {
    console.log(req.body);

    if (!apiUtil.validateApiParams(['products'], req.body)) {
        res.status(400).send('Bad request')
        return
    }

    var options = {
        mode: 'text',
        pythonOptions: ['-u'],
        args: [req.body.products]
    };
    console.log(options);

    var pyshell = new PythonShell("/app/crawler/crawl_web_vnreview.py",options);

    pyshell.on('message', function (message) {
        // received a message sent from the Python script (a simple "print" statement)
        console.log(message);
        if(message.startsWith("data_temp")) {
            var results  = require('../../data_temp_'+req.body.products);
            console.log(results);
            res.status(200).send(results);
        }
    });

//end the input stream and allow the process to exit
    pyshell.end(function (err) {
        if (err){
            throw err;
        };

        console.log('finished');
    });

}