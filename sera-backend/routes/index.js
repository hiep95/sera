const express = require('express')

module.exports = (app,io) => {
    // API router
    app.all('/api/:module/:action', (req, res, next) => {
        const {module, action} = req.params
        const method = req.method

        const app = require(`../app/${module}/apis`)
        const functionName = `API_${method}_${action}`.toUpperCase()
        if (!method || !app.hasOwnProperty(functionName)) {
            fireApiNotFound(res);
            return
        }

        // Call module api handler
        app[functionName](req, res, next)
    })

    // Authentication
    require('../app/auth')(app)
}

function fireApiNotFound(res) {
    res.status(404).send({status: 'Failed', message: 'API not found'})
}