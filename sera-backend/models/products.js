

const Sequelize = require('sequelize')
  module.exports = sequelize.define('products', {
    productId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'product_id'
    },
    producerId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'producers',
        key: 'producer_id'
      },
      field: 'producer_id'
    },
    productCategoryId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'product_categories',
        key: 'product_category_id'
      },
      field: 'product_category_id'
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      field: 'name'
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true,
      field: 'description'
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'created_at'
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'updated_at'
    },
    image: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'image'
    },
    good: {
      type: Sequelize.TEXT,
      allowNull: true,
      field: 'good'
    },
    bad: {
      type: Sequelize.TEXT,
      allowNull: true,
      field: 'bad'
    },
    price: {
      type: Sequelize.TEXT,
      allowNull: true,
      field: 'price'
    },
    statusActive: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: '1',
      field: 'status_active'
    },
  }, {
    tableName: 'products'
  });

