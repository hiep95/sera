

const Sequelize = require('sequelize')
  module.exports = sequelize.define('comments', {
    commentId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'comment_id'
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'user_id'
      },
      field: 'user_id'
    },
    productId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'products',
        key: 'product_id'
      },
      field: 'product_id'
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false,
      field: 'content'
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'created_at'
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'updated_at'
    },
    rating: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: 0,
      field: 'rating'
    },
    sourceId: {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
          model: 'sources',
          key: 'source_id'
      },
      field: 'source_id'
    },
    username: {
        type: Sequelize.STRING,
        allowNull: true,
        field: 'username'
    }
  }, {
    tableName: 'comments'
  });

