

const Sequelize = require('sequelize')
  module.exports = sequelize.define('productCategories', {
    productCategoryId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'product_category_id'
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      field: 'name'
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true,
      field: 'description'
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'created_at'
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'updated_at'
    }
  }, {
    tableName: 'product_categories'
  });

