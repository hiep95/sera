

const Sequelize = require('sequelize')
  module.exports = sequelize.define('producers', {
    producerId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'producer_id'
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      field: 'name'
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'created_at'
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'updated_at'
    }
  }, {
    tableName: 'producers'
  });

