/* jshint indent: 2 */

const Sequelize = require('sequelize')
module.exports = sequelize.define('sources', {
    name: {
        type: Sequelize.STRING,
        allowNull: true,
        field: 'name'
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: true,
        field: 'description'
    },
    sourceId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'source_id'
    }
}, {
    tableName: 'sources'
})
