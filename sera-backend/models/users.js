

const Sequelize = require('sequelize')
  module.exports = sequelize.define('users', {
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'user_id'
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      field: 'name'
    },
    email: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'email'
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'created_at'
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'updated_at'
    },
    googleId: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'google_id'
    },
    facebookId: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'facebook_id'
    },
    avatar: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: '/imgs/noavatar.png',
      field: 'avatar'
    },
    role: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: 'guest',
      field: 'role'
      }
  }, {
    tableName: 'users'
  });

