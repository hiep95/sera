

const Sequelize = require('sequelize')
  module.exports = sequelize.define('parameters', {
    parameterId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'parameter_id'
    },
    parameterCategoryId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'parameter_categories',
        key: 'parameter_category_id'
      },
      field: 'parameter_category_id'
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false,
      field: 'content'
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'created_at'
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'updated_at'
    },
    productId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'products',
        key: 'product_id'
      },
      field: 'product_id'
    }
  }, {
    tableName: 'parameters'
  });

