

const Sequelize = require('sequelize')
  module.exports = sequelize.define('reviewCategories', {
    reviewCategoryId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'review_category_id'
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      field: 'name'
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true,
      field: 'description'
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'created_at'
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'updated_at'
    }
  }, {
    tableName: 'review_categories'
  });

