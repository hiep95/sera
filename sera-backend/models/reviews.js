

const Sequelize = require('sequelize')
  module.exports = sequelize.define('reviews', {
    reviewId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'review_id'
    },
    reviewCategoryId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'review_categories',
        key: 'review_category_id'
      },
      field: 'review_category_id'
    },
    adminId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'admins',
        key: 'admin_id'
      },
      field: 'admin_id'
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false,
      field: 'content'
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'created_at'
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'updated_at'
    },
    productId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'products',
        key: 'product_id'
      },
      field: 'product_id'
    }
  }, {
    tableName: 'reviews'
  });

