

const Sequelize = require('sequelize')
  module.exports = sequelize.define('admins', {
    adminId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'admin_id'
    },
    adminName: {
      type: Sequelize.STRING,
      allowNull: false,
      field: 'admin_name'
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
      field: 'password'
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'created_at'
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: sequelize.fn('now'),
      field: 'updated_at'
    }
  }, {
    tableName: 'admins'
  });

