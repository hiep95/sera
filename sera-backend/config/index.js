const Sequelize = require('sequelize')

// Database
const {POSTGRES_DB, POSTGRES_USER, POSTGRES_HOST, POSTGRES_PASS, DATABASE_URL} = process.env
let sequelize
if (DATABASE_URL) {
    sequelize = new Sequelize(DATABASE_URL, {
        operatorsAliases: false
    })
} else {
    sequelize = new Sequelize(POSTGRES_DB, POSTGRES_USER, POSTGRES_PASS, {
        host: POSTGRES_HOST,
        dialect: 'postgres',
        operatorsAliases: false
    })
}
global.sequelize = sequelize
global.postgresQuery = function(query, replacements) {
    return sequelize.query(query, {type: sequelize.QueryTypes.SELECT, replacements: replacements})
}