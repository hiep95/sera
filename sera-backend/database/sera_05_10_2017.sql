--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admins; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE admins (
    admin_id integer NOT NULL,
    admin_name character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE admins OWNER TO postgres;

--
-- Name: admins_admin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admins_admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admins_admin_id_seq OWNER TO postgres;

--
-- Name: admins_admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admins_admin_id_seq OWNED BY admins.admin_id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE comments (
    comment_id integer NOT NULL,
    user_id integer NOT NULL,
    product_id integer NOT NULL,
    content text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE comments OWNER TO postgres;

--
-- Name: comments_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE comments_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE comments_comment_id_seq OWNER TO postgres;

--
-- Name: comments_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE comments_comment_id_seq OWNED BY comments.comment_id;


--
-- Name: manufacturies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE manufacturies (
    manufactury_id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE manufacturies OWNER TO postgres;

--
-- Name: manufacturies_manufactury_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturies_manufactury_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturies_manufactury_id_seq OWNER TO postgres;

--
-- Name: manufacturies_manufactury_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE manufacturies_manufactury_id_seq OWNED BY manufacturies.manufactury_id;


--
-- Name: product_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_categories (
    product_category_id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE product_categories OWNER TO postgres;

--
-- Name: product_categories_product_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_categories_product_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_categories_product_category_id_seq OWNER TO postgres;

--
-- Name: product_categories_product_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_categories_product_category_id_seq OWNED BY product_categories.product_category_id;


--
-- Name: product_ratings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_ratings (
    product_rating_id integer NOT NULL,
    product_id integer,
    rating integer
);


ALTER TABLE product_ratings OWNER TO postgres;

--
-- Name: product_ratings_product_rating_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_ratings_product_rating_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_ratings_product_rating_id_seq OWNER TO postgres;

--
-- Name: product_ratings_product_rating_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_ratings_product_rating_id_seq OWNED BY product_ratings.product_rating_id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products (
    product_id integer NOT NULL,
    manufactury_id integer NOT NULL,
    product_category_id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE products OWNER TO postgres;

--
-- Name: products_product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_product_id_seq OWNER TO postgres;

--
-- Name: products_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_product_id_seq OWNED BY products.product_id;


--
-- Name: review_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE review_categories (
    review_category_id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE review_categories OWNER TO postgres;

--
-- Name: review_categories_review_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE review_categories_review_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE review_categories_review_category_id_seq OWNER TO postgres;

--
-- Name: review_categories_review_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE review_categories_review_category_id_seq OWNED BY review_categories.review_category_id;


--
-- Name: reviews; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE reviews (
    review_id integer NOT NULL,
    review_category_id integer NOT NULL,
    admin_id integer NOT NULL,
    content text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE reviews OWNER TO postgres;

--
-- Name: reviews_review_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE reviews_review_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reviews_review_id_seq OWNER TO postgres;

--
-- Name: reviews_review_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE reviews_review_id_seq OWNED BY reviews.review_id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    user_id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255),
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    role character varying(10) DEFAULT 'member'::character varying,
    google_id character varying(255),
    facebook_id character varying(255)
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_user_id_seq OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_user_id_seq OWNED BY users.user_id;


--
-- Name: admins admin_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admins ALTER COLUMN admin_id SET DEFAULT nextval('admins_admin_id_seq'::regclass);


--
-- Name: comments comment_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comments ALTER COLUMN comment_id SET DEFAULT nextval('comments_comment_id_seq'::regclass);


--
-- Name: manufacturies manufactury_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manufacturies ALTER COLUMN manufactury_id SET DEFAULT nextval('manufacturies_manufactury_id_seq'::regclass);


--
-- Name: product_categories product_category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_categories ALTER COLUMN product_category_id SET DEFAULT nextval('product_categories_product_category_id_seq'::regclass);


--
-- Name: product_ratings product_rating_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_ratings ALTER COLUMN product_rating_id SET DEFAULT nextval('product_ratings_product_rating_id_seq'::regclass);


--
-- Name: products product_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products ALTER COLUMN product_id SET DEFAULT nextval('products_product_id_seq'::regclass);


--
-- Name: review_categories review_category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY review_categories ALTER COLUMN review_category_id SET DEFAULT nextval('review_categories_review_category_id_seq'::regclass);


--
-- Name: reviews review_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reviews ALTER COLUMN review_id SET DEFAULT nextval('reviews_review_id_seq'::regclass);


--
-- Name: users user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN user_id SET DEFAULT nextval('users_user_id_seq'::regclass);


--
-- Data for Name: admins; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admins (admin_id, admin_name, password, created_at, updated_at) FROM stdin;
\.


--
-- Name: admins_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admins_admin_id_seq', 1, false);


--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY comments (comment_id, user_id, product_id, content, created_at, updated_at) FROM stdin;
\.


--
-- Name: comments_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('comments_comment_id_seq', 1, false);


--
-- Data for Name: manufacturies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY manufacturies (manufactury_id, name, created_at, updated_at) FROM stdin;
1	Apple	2017-10-01 14:12:34.560543+07	2017-10-01 14:12:34.560543+07
\.


--
-- Name: manufacturies_manufactury_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturies_manufactury_id_seq', 1, true);


--
-- Data for Name: product_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_categories (product_category_id, name, description, created_at, updated_at) FROM stdin;
1	Phone	\N	2017-10-01 14:12:51.2727+07	2017-10-01 14:12:51.2727+07
\.


--
-- Name: product_categories_product_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_categories_product_category_id_seq', 1, true);


--
-- Data for Name: product_ratings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_ratings (product_rating_id, product_id, rating) FROM stdin;
2	1	6
3	2	7
\.


--
-- Name: product_ratings_product_rating_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_ratings_product_rating_id_seq', 3, true);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products (product_id, manufactury_id, product_category_id, name, description, created_at, updated_at) FROM stdin;
1	1	1	iPhone 6	\N	2017-10-01 14:13:06.8583+07	2017-10-01 14:13:06.8583+07
2	1	1	iPhone 7	\N	2017-10-01 14:13:06.8583+07	2017-10-01 14:13:06.8583+07
3	1	1	iPhone 8	\N	2017-10-01 14:13:06.8583+07	2017-10-01 14:13:06.8583+07
\.


--
-- Name: products_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_product_id_seq', 3, true);


--
-- Data for Name: review_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY review_categories (review_category_id, name, description, created_at, updated_at) FROM stdin;
\.


--
-- Name: review_categories_review_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('review_categories_review_category_id_seq', 1, false);


--
-- Data for Name: reviews; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY reviews (review_id, review_category_id, admin_id, content, created_at, updated_at, product_id) FROM stdin;
\.


--
-- Name: reviews_review_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('reviews_review_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (user_id, name, email, created_at, updated_at, role, google_id, facebook_id) FROM stdin;
4	Hiệp Trần	hiep95@gmail.com	2017-10-04 23:57:34.479763+07	2017-10-04 23:57:34.479763+07	member	100635016278203235958	\N
\.


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_user_id_seq', 4, true);


--
-- Name: admins admins_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admins
    ADD CONSTRAINT admins_pkey PRIMARY KEY (admin_id);


--
-- Name: comments comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (comment_id);


--
-- Name: manufacturies manufacturies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manufacturies
    ADD CONSTRAINT manufacturies_pkey PRIMARY KEY (manufactury_id);


--
-- Name: product_categories product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_categories
    ADD CONSTRAINT product_categories_pkey PRIMARY KEY (product_category_id);


--
-- Name: product_ratings product_ratings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_ratings
    ADD CONSTRAINT product_ratings_pkey PRIMARY KEY (product_rating_id);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (product_id);


--
-- Name: review_categories review_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY review_categories
    ADD CONSTRAINT review_categories_pkey PRIMARY KEY (review_category_id);


--
-- Name: reviews reviews_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (review_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: admins_admin_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX admins_admin_id_uindex ON admins USING btree (admin_id);


--
-- Name: admins_admin_name_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX admins_admin_name_uindex ON admins USING btree (admin_name);


--
-- Name: comments_comment_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX comments_comment_id_uindex ON comments USING btree (comment_id);


--
-- Name: manufacturies_manufactury_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX manufacturies_manufactury_id_uindex ON manufacturies USING btree (manufactury_id);


--
-- Name: manufacturies_name_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX manufacturies_name_uindex ON manufacturies USING btree (name);


--
-- Name: product_categories_name_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX product_categories_name_uindex ON product_categories USING btree (name);


--
-- Name: product_categories_product_category_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX product_categories_product_category_id_uindex ON product_categories USING btree (product_category_id);


--
-- Name: product_ratings_product_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX product_ratings_product_id_uindex ON product_ratings USING btree (product_id);


--
-- Name: product_ratings_product_rating_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX product_ratings_product_rating_id_uindex ON product_ratings USING btree (product_rating_id);


--
-- Name: products_name_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX products_name_uindex ON products USING btree (name);


--
-- Name: products_product_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX products_product_id_uindex ON products USING btree (product_id);


--
-- Name: review_categories_name_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX review_categories_name_uindex ON review_categories USING btree (name);


--
-- Name: review_categories_review_category_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX review_categories_review_category_id_uindex ON review_categories USING btree (review_category_id);


--
-- Name: reviews_review_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX reviews_review_id_uindex ON reviews USING btree (review_id);


--
-- Name: users_email_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX users_email_uindex ON users USING btree (email);


--
-- Name: users_user_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX users_user_id_uindex ON users USING btree (user_id);


--
-- Name: comments comments_products_product_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_products_product_id_fk FOREIGN KEY (product_id) REFERENCES products(product_id);


--
-- Name: comments comments_users_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_users_user_id_fk FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: product_ratings product_ratings_products_product_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_ratings
    ADD CONSTRAINT product_ratings_products_product_id_fk FOREIGN KEY (product_id) REFERENCES products(product_id);


--
-- Name: products products_manufacturies_manufactury_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_manufacturies_manufactury_id_fk FOREIGN KEY (manufactury_id) REFERENCES manufacturies(manufactury_id);


--
-- Name: products products_product_categories_product_category_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_product_categories_product_category_id_fk FOREIGN KEY (product_category_id) REFERENCES product_categories(product_category_id);


--
-- Name: reviews reviews_admins_admin_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT reviews_admins_admin_id_fk FOREIGN KEY (admin_id) REFERENCES admins(admin_id);


--
-- Name: reviews reviews_products_product_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT reviews_products_product_id_fk FOREIGN KEY (product_id) REFERENCES products(product_id);


--
-- Name: reviews reviews_review_categories_review_category_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT reviews_review_categories_review_category_id_fk FOREIGN KEY (review_category_id) REFERENCES review_categories(review_category_id);


--
-- PostgreSQL database dump complete
--

