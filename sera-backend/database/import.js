var data  = require('./data_insert_9_12_2017');
const Sequelize = require('sequelize')

const {POSTGRES_DB, POSTGRES_USER, POSTGRES_HOST, POSTGRES_PASS} = process.env
const sequelize = new Sequelize('sera', 'postgres', 'postgres', {
    host: 'localhost',
    dialect: 'postgres',
});

for(var i = 0; i < data.length; i++) {
    let name = data[i].name
    let product_category = data[i].product_category
    let producer = data[i].producer
    let description = data[i].description||''
    let image = data[i].image
    let p = JSON.stringify(data[i].parameter)
    let r = JSON.stringify(data[i].review)
    let good = data[i].goodbad.good
    let bad = data[i].goodbad.bad
    let price = data[i].goodbad.price||''
    let comment = JSON.stringify(data[i].comment)
    let source = data[i].soure
    sequelize.query('SELECT * from addProduct(:name, :product_category, :producer, :description, :image, :p, :r, :good, :bad, :price, :comment, :source)',
        {type: sequelize.QueryTypes.SELECT, replacements: {name, product_category, producer, description, image, p, r, good, bad, price, comment, source}})
}