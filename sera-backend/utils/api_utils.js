exports.validateApiParams = function validateApiParams(requirements, params) {
    return requirements.every(prop => !!params[prop])
}