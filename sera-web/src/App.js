import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getCurrentUser} from './actions/AuthAction'
import {BrowserRouter, Route} from 'react-router-dom'
import Header from './components/Header'
import ProductSearchList from './components/ProductSearchList'
import ProductDetail from './components/products/ProductDetail'
import Home from './components/Home'
import SearchBar from './components/SearchBar'
import ProductNotFound from "./components/products/ProductNotFound"
import SomethingWentWrong from "./components/SomethingWentWrong";

import CreateProductForm from './components/products/CreateProductForm';
import EditProductForm from './components/products/EditProductForm';
import Analytic from './components/admin/Analytic';
import Crawler from './components/admin/Crawler';
import PersistentDrawer from './components/PersistentDrawer';

import {withStyles} from 'material-ui/styles';


const drawerWidth = 240;

const styles = theme => ({
    paper: {
        height: 'calc(100% - 64px)',
        top: 64,
        backgroundColor: '#007b6e',
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
    },
    root: {
        width: '100%',
        height: 430,
        marginTop: theme.spacing.unit * 3,
        zIndex: 1,
        overflow: 'hidden',
    },
    appFrame: {
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100%',
    },
    appBar: {
        position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'appBarShift-left': {
        marginLeft: drawerWidth,
    },
    'appBarShift-right': {
        marginRight: drawerWidth,
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        marginTop: 64,
        position: 'relative',
        height: '100%',
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        width: '100%',
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        height: 'calc(100% - 56px)',
        marginTop: 56,
        [theme.breakpoints.up('sm')]: {
            content: {
                height: 'calc(100% - 64px)',
                marginTop: 64,
            },
        },
    },
    'content-left': {
        marginLeft: -drawerWidth,
    },
    'content-right': {
        marginRight: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShift-left': {
        marginLeft: 0,
    },
    'contentShift-right': {
        marginRight: 0,
    },
});

class App extends Component {
    componentDidMount() {
        this.props.getCurrentUser()
    }

    render() {
        return (
                <BrowserRouter>
                    <div>
                        <Header />
                        <div className="container-fluid" style={{marginTop: 0}}>
                            <Route exact path='/' component={Home} />
                            <Route exact path="/admin/crawler" component={Crawler} />
                            <Route exact path='/search/:term' component={ProductSearchList} />
                            <Route exact path='/search/:type/:term' component={ProductSearchList} />

                            <Route exact path='/products/:prodName' component={ProductDetail} />
                            <Route exact path='/product_not_found' component={ProductNotFound} />

                            <Route exact path='/something_went_wrong' component={SomethingWentWrong} />

                            <Route exact path="/admin/product/new" component={CreateProductForm} />
                            <Route exact path="/admin/product/:id/edit" component={EditProductForm} />
                            <Route exact path="/admin/analytic" component={Analytic} />
                            <Route exact path="/drawer" component={PersistentDrawer} />
                        </div>
                    </div>
                </BrowserRouter>
        )
    }
}

export default connect(null, {getCurrentUser})(withStyles(styles, { withTheme: true })(App))
