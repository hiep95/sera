import {
    CRAWL_SUCCESS
} from '../actions/types'

const INIT_STATE = {
    crawlResults: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case CRAWL_SUCCESS:
            return {...state, crawlResults: action.payload}
        default:
            return state
    }
}