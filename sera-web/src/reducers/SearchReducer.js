import {
    SEARCH_PRODUCT_SUCCESS,
    SEARCH_TERM_CHANGED,
} from '../actions/types'

const INIT_STATE = {
    products: [],
    term: ''
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SEARCH_PRODUCT_SUCCESS:
            return {...state, products: [...action.payload]}
        case SEARCH_TERM_CHANGED:
            return {...state, term: action.payload}
        default: 
            return state   
    }
}