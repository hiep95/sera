import {combineReducers} from 'redux'
import SearchReducer from './SearchReducer'
import AuthReducer from './AuthReducer'
import CommentReducer from './CommentReducer'
import ProductReducer from "./ProductReducer"
import ProducerReducer from "./ProducerReducer"
import AdminReducer from "./AdminReducer"

export default combineReducers({
    search: SearchReducer,
    auth: AuthReducer,
    comments: CommentReducer,
    product: ProductReducer,
    producer: ProducerReducer,
    admin: AdminReducer
})