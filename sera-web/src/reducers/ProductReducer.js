import {
    FETCH_PRODUCT_PARAMS_SUCCESS, FETCH_PRODUCT_REVIEW_SUCCESS,
    FETCH_PRODUCT_SUCCESS, ON_PRODUCT_SELECTED,
    ALL_PRODUCERS_SUCCESS,
    ALL_PARAMETER_CATEGORY_SUCCESS,
    ALL_REVIEW_CATEGORY_SUCCESS,
    ALL_PRODUCT_CATEGORY_SUCCESS,
    ALL_PRODUCT_SUCCESS,
    ADD_PRODUCT_SUCCESS,
    GET_PRODUCT_SUCCESS, CLEAR_PRODUCT_STORE_DATA, FETCH_PRODUCT_RATING_SUCCESS
} from '../actions/types'

const INIT_STATE = {
    currentProduct: null,
    params: null,
    review: null,
    rating: null,
    product: {},
    products: [],
    producers: [],
    parameterCategories: [],
    reviewCategories: [],
    productCategories: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case FETCH_PRODUCT_SUCCESS:
            return {...state, currentProduct: action.payload}
        case ON_PRODUCT_SELECTED:
            return {...state, currentProduct: action.payload}
        case FETCH_PRODUCT_PARAMS_SUCCESS:
            return {...state, params: action.payload}
        case FETCH_PRODUCT_REVIEW_SUCCESS:
            return {...state, review: action.payload}
        case FETCH_PRODUCT_RATING_SUCCESS:
            return {...state, rating: action.payload}
        case ALL_PRODUCERS_SUCCESS:
            return {...state, producers: [...action.payload]}
        case ALL_PARAMETER_CATEGORY_SUCCESS:
            return {...state, parameterCategories: [...action.payload]}
        case ALL_REVIEW_CATEGORY_SUCCESS:
            return {...state, reviewCategories: [...action.payload]}
        case ALL_PRODUCT_CATEGORY_SUCCESS:
            return {...state, productCategories: [...action.payload]}
        case ALL_PRODUCT_SUCCESS:
            return {...state, products: [...action.payload]}
        case GET_PRODUCT_SUCCESS:
            return {...state, product: [...action.payload]}
        case CLEAR_PRODUCT_STORE_DATA:
            return {...state, params: null, rating: null}
        default: 
            return state   
    }
}