import {
    ALL_PRODUCERS_SUCCESS
} from '../actions/types'

const INIT_STATE = {
    producers: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case ALL_PRODUCERS_SUCCESS:
            return {...state, producers: [...action.payload]}
        default:
            return state
    }
}