import {
    COMMENT_TEXT_CHANGE,
    FETCH_COMMENT_SUCCESS, POST_COMMENT_SUCCESS
} from '../actions/types'

const INIT_STATE = {
    comments: null,
    commentText: '',
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case FETCH_COMMENT_SUCCESS:
            return {...state, comments: action.payload}
        case POST_COMMENT_SUCCESS:
            return {...state, comments: [action.payload, ...state.comments], commentText: ''}
        case COMMENT_TEXT_CHANGE:
            return {...state, commentText: action.payload}
        default: 
            return state
    }
}