import {
    GET_CURRENT_USER_SUCCESS,
} from '../actions/types'

const INIT_STATE = {
    user: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CURRENT_USER_SUCCESS:
            return {...state, user: action.payload || false}
        default:
            return state
    }
}