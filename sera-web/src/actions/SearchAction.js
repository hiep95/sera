import axios from 'axios'
import * as Apis from './apis'
import {
    SEARCH_PRODUCT_SUCCESS,
    SEARCH_TERM_CHANGED,
} from './types'

export function searchProduct(name) {
    return dispatch => {
        axios.get(Apis.SEARCH_PRODUCTS, {
            params: {q: name}
        })
            .then(({data}) => {
                dispatch({
                    type: SEARCH_PRODUCT_SUCCESS,
                    payload: data
                })
            })
            .catch(err => console.error(err))
    }
}

export function changeSearchTerm(term) {
    return {
        type: SEARCH_TERM_CHANGED,
        payload: term
    }
}

export function searchProductsByProducerId(producerId) {
    return async dispatch => {
        const {data} = await axios.get(Apis.GET_PRODUCTS_BY_PRODUCER, {params: {producer_id: producerId}})
        dispatch({
            type: SEARCH_PRODUCT_SUCCESS,
            payload: data
        })
    }
}

export function searchProductsByProducerName(producerName) {
    return async dispatch => {
        const {data} = await axios.get(Apis.GET_PRODUCTS_BY_PRODUCER, {params: {producer_name: producerName}})
        dispatch({
            type: SEARCH_PRODUCT_SUCCESS,
            payload: data
        })
    }
}