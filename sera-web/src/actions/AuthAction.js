import axios from 'axios'
import {
    GET_CURRENT_USER
} from './apis'
import {
    GET_CURRENT_USER_SUCCESS
} from "./types";

export function getCurrentUser() {
    return async dispatch => {
        const {data} = await axios.get(GET_CURRENT_USER)
        dispatch({
            type: GET_CURRENT_USER_SUCCESS,
            payload: data
        })
    }
}