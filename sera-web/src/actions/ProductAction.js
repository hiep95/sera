import axios from 'axios'
import * as Apis from './apis'
import { browserHistory } from 'react-router';
import {
    CLEAR_PRODUCT_STORE_DATA,
    FETCH_PRODUCT_PARAMS_SUCCESS, FETCH_PRODUCT_RATING_SUCCESS,
    ON_PRODUCT_SELECTED,
    FETCH_PRODUCT_REVIEW_SUCCESS, FETCH_PRODUCT_SUCCESS,
    DELETE_PRODUCT_SUCCESS
} from "./types";
import {GET_PRODUCT, GET_PRODUCT_PARAMETERS, GET_PRODUCT_REVIEW, GET_PRODUCTS_RATING} from "./apis";

export function fetchProductById(productId) {
    return async dispatch => {
        const {data} = await axios.get(GET_PRODUCT, {params: {
            id: productId
        }})
        dispatch({
            type: FETCH_PRODUCT_SUCCESS,
            payload: data
        })
    }
}

export function clearViewingProduct() {
    return {
        type: ON_PRODUCT_SELECTED,
        payload: null
    }
}

export function onSelectProduct(product) {
    return async dispatch => {
        let data
        if (!isNaN(product)) { // Treat as product id
            data = (await axios.get(GET_PRODUCT, {params: {
                id: product
            }})).data
        } else { // Product object
            data = product
        }
        return dispatch({
            type: ON_PRODUCT_SELECTED,
            payload: data || false
        })
    }
}

export function getProductParams(productId) {
    return async dispatch => {
        const {data} = await axios.get(GET_PRODUCT_PARAMETERS, {params: {product_id: productId}})
        dispatch({
            type: FETCH_PRODUCT_PARAMS_SUCCESS,
            payload: data
        })
    }
}

export function clearProductParams(productId) {
    return {
        type: FETCH_PRODUCT_PARAMS_SUCCESS,
        payload: null
    }
}

export function onExitProductDetail(productId) {
    return {
        type: CLEAR_PRODUCT_STORE_DATA,
    }
}

export function getProductReview(productId) {
    return async dispatch => {
        const {data} = await axios.get(GET_PRODUCT_REVIEW, {params: {product_id: productId}})
        dispatch({
            type: FETCH_PRODUCT_REVIEW_SUCCESS,
            payload: data
        })
    }
}

export function getProductRating(productId) {
    return async dispatch => {
        const {data} = await axios.get(GET_PRODUCTS_RATING, {params: {product_id: productId}})
        dispatch({
            type: FETCH_PRODUCT_RATING_SUCCESS,
            payload: data
        })
    }
}

export function clearProductReview(productId) {
    return {
        type: FETCH_PRODUCT_REVIEW_SUCCESS,
        payload: null
    }
}

// export function deleteProduct(input) {
//     console.log(input)
//     return dispatch => {
//         axios.post(Apis.POST_DELETE_PRODUCT, input)
//             .then(({data}) => {
//                 console.log(data)
//                 dispatch({
//                     type: DELETE_PRODUCT_SUCCESS,
//                     payload: data
//                 })
//             }).then(
//                 //reload
//         // )
//             .catch(err => console.error(err))
//     }
// }