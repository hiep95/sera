const API_URL = '/api'
export const GET_ALL_PRODUCTS = `${API_URL}/products/all`
export const GET_PRODUCT = `${API_URL}/products/get`
export const SEARCH_PRODUCTS = `${API_URL}/products/search`
export const GET_PRODUCT_PARAMETERS = `${API_URL}/products/parameters`
export const GET_PRODUCT_REVIEW = `${API_URL}/products/review`
export const GET_TOP_PRODUCTS = `${API_URL}/products/top`
export const GET_ALL_PRODUCERS = `${API_URL}/products/all_producers`
export const GET_PRODUCTS_BY_PRODUCER = `${API_URL}/products/products_by_producer`
export const GET_PRODUCTS_RATING = `${API_URL}/products/rating`

// Auth
export const GET_CURRENT_USER = `${API_URL}/auth/me`

// Comment
export const GET_COMMENTS = `${API_URL}/comments/all`
export const POST_COMMENTS = `${API_URL}/comments/new`

//Admin
export const GET_PARAMETER_CATEGORY = `${API_URL}/products/parameter_category`
export const GET_REVIEW_CATEGORY = `${API_URL}/products/review_category`
export const GET_PRODUCT_CATEGORY = `${API_URL}/products/product_category`
export const POST_ADD_PRODUCT = `${API_URL}/products/add_product`
export const POST_EDIT_PRODUCT = `${API_URL}/products/edit_product`
export const GET_GET_PRODUCT = `${API_URL}/products/get_product`
export const POST_DELETE_PRODUCT = `${API_URL}/products/delete_product`
export const GET_NUMBER_OF_COMMENT = `${API_URL}/analytic/number_of_comment`
export const POST_CRAWL = `${API_URL}/admin/crawl`

//Crawl