import axios from 'axios'
import * as Apis from './apis'
import {
    ALL_PRODUCT_SUCCESS,
    ALL_PRODUCERS_SUCCESS,
    ALL_PARAMETER_CATEGORY_SUCCESS,
    ALL_REVIEW_CATEGORY_SUCCESS,
    ALL_PRODUCT_CATEGORY_SUCCESS,
    ADD_PRODUCT_SUCCESS,
    EDIT_PRODUCT_SUCCESS,
    GET_PRODUCT_SUCCESS,
    DELETE_PRODUCT_SUCCESS,
    CRAWL_SUCCESS
} from './types'

export function allProduct() {
    return dispatch => {
        axios.get(Apis.GET_ALL_PRODUCTS, {
            params: {}
        })
            .then(({data}) => {
                dispatch({
                    type: ALL_PRODUCT_SUCCESS,
                    payload: data
                })
            })
            .catch(err => console.error(err))
    }
}

export function getProduct(id) {
    return dispatch => {
        axios.get(Apis.GET_PRODUCT, {
            params: {id: id}
        })
            .then(({data}) => {
                console.log(data)
                dispatch({
                    type: GET_PRODUCT_SUCCESS,
                    payload: data
                })
            })
            .catch(err => console.error(err))
    }
}

export function allProducer() {
    console.log('da vao day')
    return dispatch => {
        axios.get(Apis.GET_ALL_PRODUCERS, {
            params: {}
        })
            .then(({data}) => {
            console.log(data)
                dispatch({
                    type: ALL_PRODUCERS_SUCCESS,
                    payload: data
                })
            })
            .catch(err => console.error(err))
    }
}

export function allParameterCategory() {
    return dispatch => {
        axios.get(Apis.GET_PARAMETER_CATEGORY, {
            params: {}
        })
            .then(({data}) => {
                dispatch({
                    type: ALL_PARAMETER_CATEGORY_SUCCESS,
                    payload: data
                })
            })
            .catch(err => console.error(err))
    }
}

export function allReviewCategory() {
    return dispatch => {
        axios.get(Apis.GET_REVIEW_CATEGORY, {
            params: {}
        })
            .then(({data}) => {
                dispatch({
                    type: ALL_REVIEW_CATEGORY_SUCCESS,
                    payload: data
                })
            })
            .catch(err => console.error(err))
    }
}

export function allProductCategory() {
    return dispatch => {
        axios.get(Apis.GET_PRODUCT_CATEGORY, {
            params: {}
        })
            .then(({data}) => {
                dispatch({
                    type: ALL_PRODUCT_CATEGORY_SUCCESS,
                    payload: data
                })
            })
            .catch(err => console.error(err))
    }
}

export function addProduct(input) {
    console.log(input)
    return dispatch => {
        axios.post(Apis.POST_ADD_PRODUCT, input)
            .then(({data}) => {
                console.log(data)
                dispatch({
                    type: ADD_PRODUCT_SUCCESS,
                    payload: data
                })
            }).then()
            .catch(err => console.error(err))
    }
}

export function editProduct(input) {
    console.log(input)
    return dispatch => {
        axios.post(Apis.POST_EDIT_PRODUCT, input)
            .then(({data}) => {
                console.log(data)
                dispatch({
                    type: EDIT_PRODUCT_SUCCESS,
                    payload: data
                })
            }).then()
            .catch(err => console.error(err))
    }
}

export function deleteProduct(input) {
    console.log(input)
    return dispatch => {
        axios.post(Apis.POST_DELETE_PRODUCT, input)
            .then(({data}) => {
                console.log(data)
                dispatch({
                    type: DELETE_PRODUCT_SUCCESS,
                    payload: data
                })
            }).then()
            .catch(err => console.error(err))
    }
}

export function crawl(input) {
    //console.log(input)
    return dispatch => {
        axios.post(Apis.POST_CRAWL, input)
            .then(({data}) => {
                //console.log(data)
                dispatch({
                    type: CRAWL_SUCCESS,
                    payload: data
                })
            }).then()
            .catch(err => console.error(err))
    }
}