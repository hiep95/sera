import axios from 'axios'
import {
    GET_COMMENTS, POST_COMMENTS
} from "./apis"
import {
    COMMENT_TEXT_CHANGE,
    FETCH_COMMENT_SUCCESS, POST_COMMENT_SUCCESS
} from "./types"

export function fetchComments(productId) {
    return async dispatch => {
        dispatch({
            type: FETCH_COMMENT_SUCCESS,
            payload: null
        })

        const {data} = await axios.get(GET_COMMENTS, {
            params: {product_id: productId}
        })
        data.map(comment => {
            if (comment.rating) comment.rating /= 10
        })

        dispatch({
            type: FETCH_COMMENT_SUCCESS,
            payload: data
        })
    }
}

export function onCommentTextChange(text) {
    return {
        type: COMMENT_TEXT_CHANGE,
        payload: text
    }
}

export function postComment(productId, comment, rating) {
    return async dispatch => {
        const {data} = await axios.post(POST_COMMENTS, {
            params: {
                product_id: productId,
                comment_content: comment,
                rating: (rating > 0 && rating <= 10) ? rating * 10 : undefined
            }
        })
        if (data.rating) data.rating /= 10

        dispatch({
            type: POST_COMMENT_SUCCESS,
            payload: data
        })
    }
}