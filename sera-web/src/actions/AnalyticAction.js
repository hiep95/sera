import axios from 'axios'
import * as Apis from './apis'
import { browserHistory } from 'react-router';
import {
    GET_NUMBER_OF_COMMENT_SUCCESS
} from './types'

export function getNumberOfComment() {
    return dispatch => {
        axios.get(Apis.GET_NUMBER_OF_COMMENT, {
            params: {}
        })
            .then(({data}) => {
                let formalData = [];
                for (var i = 0; i < data.length; i++) {
                    let j = data[i].month;
                    formalData[j] = data[i].number;
                }
                console.log(formalData);
                dispatch({
                    type: GET_NUMBER_OF_COMMENT_SUCCESS,
                    payload: formalData
                })
            })
            .catch(err => console.error(err))
    }
}