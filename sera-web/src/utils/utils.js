export function inflateProductName(productName) {
    return productName.replace(/[^a-zA-Z0-9]/g, '_').replace(/_+/g, '_')
}