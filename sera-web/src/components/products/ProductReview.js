import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getProductReview, clearProductReview} from '../../actions/ProductAction'
import $ from 'jquery';

class ProductReview extends Component {
    componentWillMount() {
        this.props.getProductReview(this.props.productId)
    }

    componentWillUnmount() {
        this.props.clearProductReview()
    }

    renderReview() {
        if (!this.props.review) return

        return this.props.review.map(reviewOnPart => {
            return (
                <div className="review" style={{paddingTop: 100, marginTop: -70}} key={reviewOnPart.reviewId} id={reviewOnPart.reviewCategory.name.toLowerCase().replace(" ", "_")}>
                    <h5>{reviewOnPart.reviewCategory.name}</h5>
                    <div>
                        {reviewOnPart.content}
                    </div>
                    <hr/>
                </div>
            )
        })
    }

    renderMenu() {
        if (!this.props.review) return

        return this.props.review.map(reviewOnPart => {
            return (
                <li><a href={"#"+reviewOnPart.reviewCategory.name.toLowerCase().replace(" ", "_")}>{reviewOnPart.reviewCategory.name}</a></li>
            )
        })
    }

    render() {
        return (
            <div>
                <div className="col hide-on-small-only m12 sl2">
                    {this.renderReview()}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        review: state.product.review
    }
}

export default connect(mapStateToProps, {getProductReview, clearProductReview})(ProductReview)