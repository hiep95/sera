import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getProductReview, clearProductReview} from '../../actions/ProductAction'
import $ from 'jquery';

class ReviewMenu extends Component {
    componentWillMount() {
        this.props.getProductReview(this.props.productId)
    }

    componentWillUnmount() {
        this.props.clearProductReview()
    }

    conponentDidUpdate() {

    }

    renderReview() {
        if (!this.props.review) return

        return this.props.review.map(reviewOnPart => {
            return (
                <div style={{marginTop: 50}} key={reviewOnPart.reviewId} id={reviewOnPart.reviewCategory.name.toLowerCase().replace(" ", "_")}>
                    <h5>{reviewOnPart.reviewCategory.name}</h5>
                    <div>
                        {reviewOnPart.content}
                    </div>
                </div>
            )
        })
    }

    renderMenu() {
        if (!this.props.review) return

        return this.props.review.map(reviewOnPart => {
            return (
                <li><a href={"#"+reviewOnPart.reviewCategory.name.toLowerCase().replace(" ", "_")}>{reviewOnPart.reviewCategory.name}</a></li>
            )
        })
    }

    render() {
        return (

                <div className="offset-m10 col hide-on-small-only m2 sl2 fixed" style={{position: 'fixed'}}>
                    <ul className="section table-of-contents">
                        {this.renderMenu()}
                    </ul>
                </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        review: state.product.review
    }
}

export default connect(mapStateToProps, {getProductReview, clearProductReview})(ReviewMenu)