import React, {Component} from 'react'
import StarBar from "../common/StarBar";
import {connect} from 'react-redux'
import {Link, withRouter} from 'react-router-dom'
import {inflateProductName} from "../../utils/utils"
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import {deleteProduct, allProduct} from "../../actions/index";
import SweetAlert from 'sweetalert-react'; // eslint-disable-line import/no-extraneous-dependencies
import 'sweetalert/dist/sweetalert.css';


class ProductItem extends Component {
    state = {
        product_id: 0,
        name: '',
        producer: '',
        created_at: '',
        editHref: ''
    };

    componentDidMount(){
        const {productId, name, producer, created_at} = this.props.product
        let createAt = new Date(created_at).toLocaleString()
        let editHref = "/admin/product/"+productId+"/edit"
        this.setState({
            product_id: productId,
            name: name,
            producer: producer,
            created_at: createAt,
            editHref: editHref
        })
    }

    handleDeleteItem() {
        console.log('vao de')
        this.props.deleteProduct({product_id: this.state.product_id})
        this.setState({showSuccess: true})
        setTimeout(() => {
            this.setState({showSuccess: false})
            window.location.reload();
        }, 2000)

    }

    _renderAdminButton() {
        const {user} = this.props;
        const productID = this.props.product.productId || this.props.product.product_id
        if(user && user.role && user.role == 'admin') {
            console.log(user);
            return (
                <div className="inline-block">
                    <Link to={`/products/${inflateProductName(this.state.name)}-${productID}`}>Read more</Link>
                <Link to={`/admin/product/${productID}/edit`}>
                    Edit
                </Link>
                <a onClick={() => this.setState({ show: true })}>Delete</a>
                    <SweetAlert
                        show={this.state.show}
                        title="Delete"
                        text={"Are you want to delete "+this.state.name}
                        type="warning"
                        showCancelButton
                        onConfirm={() => {
                            this.handleDeleteItem()
                        }}
                        onCancel={() => this.setState({ show: false })}
                    />
                    <SweetAlert
                        show={this.state.showSuccess}
                        title="Demo"
                        text="SweetAlert in React"
                        type='success'
                    />
                </div>

            )
        }else{
            return (
                <div className="inline-block">
                    <Link to={`/products/${inflateProductName(this.state.name)}-${productID}`}>Read more</Link>
                </div>
            )
        }
    }

    render() {
        const {name, description, image, rating} = this.props.product
        const productId = this.props.product.productId || this.props.product.product_id
        const imgSrc = image || require('../../images/no_image.png')

        return (

                <div className="col s12 m4">
                    <div className="card transparent z-depth-5">
                        <div className="card-image">
                            <img src={imgSrc} style={{height: 200}}/>
                            <span style={{display: 'inline-block'}} className="card-title">{name.charAt(0).toUpperCase() + name.slice(1)}
                                <StarBar score={rating / 10} style={{height: 15}}/>
                            </span>

                        </div>
                        <div className="card-content" style={{display: 'inline-block', height: 100}}>
                            <p>{description ? description.substring(0, 120)+'...' : ''}</p>
                        </div>
                        <div className="card-action">
                            {this._renderAdminButton()}
                        </div>
                    </div>
                </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.auth.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        deleteProduct(input){
            dispatch(deleteProduct(input))
        },
        allProduct(){
            dispatch(allProduct())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductItem))