import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getProductParams, clearProductParams} from '../../actions/ProductAction'
import {getProductRating, onExitProductDetail} from "../../actions";
import StarBar from "../common/StarBar";

class ProductMiniDetails extends Component {
    componentWillMount() {
        const {productId} = this.props.product
        this.props.getProductParams(productId)
        this.props.getProductRating(productId)
    }

    componentWillUnmount() {
        this.props.onExitProductDetail()
    }

    renderParams() {
        if (!this.props.params) return
        return this.props.params.map(param => {
            return (
                <div key={param.parameterId}>
                    <label>{param.parameterCategory.name}</label>
                    <div>{param.content}</div>
                </div>
            )
        })
    }

    renderRating() {
        if (!this.props.rating) return
        return this.props.rating.map((rating, index) => {
            return (
                <div key={index}>
                    <span>{rating.name}</span>
                    <StarBar style={{alignSelf: 'center', height: 12, marginLeft: 20}}
                             score={rating.rating / 10} />
                </div>
            )
        })
    }

    render() {
        const {image} = this.props.product
        return (
            <div className='row'>
                <div className='s12'>
                    <img alt='preview' style={{width: '100%', height: 'auto', objectFit: 'contain'}} src={image} />
                </div>
                <div className='s12' style={{display: 'inline', marginBottom: 60}}>
                    {this.renderRating()}
                </div>
                <div className='s12'>
                    {this.renderParams()}
                </div>
            </div>
        )
    }
}

export default connect(state => ({params: state.product.params, rating: state.product.rating}), {
    getProductParams, clearProductParams, getProductRating, onExitProductDetail
})(ProductMiniDetails)