import React, {Component} from 'react'
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {allProduct, allProducer, allParameterCategory, allProductCategory, allReviewCategory, addProduct} from "../../actions/index";
import {TextField, FormControl, Select, MenuItem, Input, InputLabel, Tab, Tabs, AppBar} from 'material-ui';
import SweetAlert from 'sweetalert-react'; // eslint-disable-line import/no-extraneous-dependencies
import 'sweetalert/dist/sweetalert.css';


function TabContainer(props) {
    return <div style={{ padding: 8 * 3 }}>{props.children}</div>;
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};

class CreateProductForm extends Component {

    state = {
        name: "",
        product_category: "phone",
        image: "",
        producer: "Apple",
        description: "",
        parameter: {},
        review: {},
        value: 0
    };

    addParameter(parameterCategory, content) {
        this.state.parameter[parameterCategory] = content
    }

    addReview(reviewCategory, content) {
        this.state.review[reviewCategory] = content
    }

    componentDidMount() {
        this.props.allProducer()
        this.props.allParameterCategory()
        this.props.allReviewCategory()
        this.props.allProductCategory()
    }

    onFormSubmit(event) {
        event.preventDefault()
        this.setState({ show: true })
    }

    handleCreateItem(){
        this.props.addProduct(this.state)
        this.setState({showSuccess: true})
        setTimeout(() => {
            this.setState({showSuccess: false})
            this.props.history.push('/')
        }, 2000)
    }

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const {classes} = this.props;
        const { value } = this.state;

        return(
            <div className="row" >
                <form id="createProduct" onSubmit={this.onFormSubmit.bind(this)}>
                    <AppBar position="static" style={{backgroundColor: '#eee', color: '#000'}}>
                        <Tabs style={{textAlign: 'center'}} value={value} onChange={this.handleChange}>
                            <Tab label="Introduction" />
                            <Tab label="Parameter" />
                            <Tab label="Review" />
                        </Tabs>
                    </AppBar>
                    <div className="tab-content">
                        {value === 0 && <TabContainer>
                            <div className="row">
                                <div className="col s12 m6">
                                    <div className="form-group">
                                        <TextField
                                            required
                                            id="name"
                                            label="Product Name"
                                            margin="normal"
                                            multiline
                                            fullWidth
                                            onChange={event => this.setState({name: event.target.value})}
                                            value={this.state.name}
                                            placeholder="Product Name"
                                        />
                                    </div>
                                </div>

                                <div className="col s12 m6">
                                    <div className="form-group">
                                        <TextField
                                            required
                                            label="Image"
                                            margin="normal"
                                            multiline
                                            fullWidth
                                            onChange={event => this.setState({image: event.target.value})}
                                            name="image"
                                            id="image"
                                            value={this.state.image}
                                            placeholder="Image Link"
                                        />

                                    </div>
                                </div>

                                <div className="col s12 m6">
                                    <div className="form-group">
                                        <FormControl fullWidth>
                                            <InputLabel htmlFor="age-simple">Producer Name</InputLabel>
                                            <Select
                                                required
                                                fullWidth
                                                value={this.state.producer}
                                                onChange={event => this.setState({producer: event.target.value})}
                                                input={<Input name="producer" id="producer" />}
                                            >
                                                {this.props.producers.map(producer => {
                                                    return (
                                                        <MenuItem
                                                            selected={producer.name==this.state.producer}
                                                            key={producer.manufactury_id}
                                                            value={producer.name}>{producer.name}</MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        </FormControl>
                                    </div>
                                </div>

                                <div className="col s12 m6">
                                    <div className="form-group">
                                        <FormControl fullWidth>
                                            <InputLabel htmlFor="age-simple">Product Category</InputLabel>
                                            <Select
                                                value={this.state.product_category}
                                                onChange={event => this.setState({product_category: event.target.value})}
                                                input={<Input name="product_category" id="product_category" />}
                                            >
                                                {this.props.productCategories.map(productCategory => {
                                                    return (
                                                        <MenuItem
                                                            selected={productCategory.name==this.state.product_category}
                                                            key={productCategory.product_category_id}
                                                            value={productCategory.name}>{productCategory.name}</MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        </FormControl>
                                    </div>
                                </div>


                                <div className="col m12">
                                    <div className="form-group">
                                        <TextField
                                            required
                                            label="Description"
                                            margin="normal"
                                            multiline
                                            fullWidth
                                            onChange={event => this.setState({description: event.target.value})}
                                            name="description"
                                            id="description"
                                            value={this.state.description}
                                            placeholder="Description"
                                        />

                                    </div>
                                </div>
                            </div>
                        </TabContainer>}
                        {value === 1 && <TabContainer>
                            <div className="row">
                                {this.props.parameterCategories.map(parameterCategory => {
                                    return (
                                        <div className="col s12 m4">
                                            <div className="form-group">
                                                <TextField
                                                    label={parameterCategory.name}
                                                    multiline
                                                    fullWidth
                                                    margin="normal"
                                                    onChange={event => this.addParameter(parameterCategory.name, event.target.value)}
                                                    name={"parameterCategory_"+parameterCategory.parameter_category_id}
                                                    id={"parameterCategory_"+parameterCategory.parameter_category_id}
                                                    placeholder={parameterCategory.name}
                                                />
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        </TabContainer>}
                        {value === 2 && <TabContainer>
                            <div className="row">
                                {this.props.reviewCategories.map(reviewCategory => {
                                    return (
                                        <div className="col s12 m6">
                                            <div className="form-group">
                                                <TextField
                                                    label={reviewCategory.name}
                                                    multiline
                                                    fullWidth
                                                    margin="normal"
                                                    onChange={event => this.addReview(reviewCategory.name, event.target.value)}
                                                    name={"reviewCategory_"+reviewCategory.review_category_id}
                                                    id={"reviewCategory_"+reviewCategory.review_category_id}
                                                    placeholder={reviewCategory.name}
                                                />
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        </TabContainer>}
                    </div>
                    <button style={{display: 'flex', justifyContent: 'center', textAlign: 'center', margin: "0 auto"}} type="submit" className="btn btn-default" form="createProduct">Submit</button>
                    <SweetAlert
                        show={this.state.show}
                        title="Add new product"
                        text={"Are you want to add "+this.state.name}
                        type="warning"
                        showCancelButton
                        onConfirm={() => {
                            this.handleCreateItem()
                        }}
                        onCancel={() => this.setState({ show: false })}
                    />
                    <SweetAlert
                        show={this.state.showSuccess}
                        title="Success"
                        text={"Add " + this.state.name + "success"}
                        type='success'
                    />
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        producers: state.product.producers,
        reviewCategories: state.product.reviewCategories,
        productCategories: state.product.productCategories,
        parameterCategories: state.product.parameterCategories
    }
}

const mapDispatchToProps = dispatch => {
    return {
        allProduct(){
            dispatch(allProduct())
        },
        allProducer(){
            dispatch(allProducer())
        },
        allReviewCategory (){
            dispatch(allReviewCategory())
        },
        allProductCategory(){
            dispatch(allProductCategory())
        },
        allParameterCategory(){
            dispatch(allParameterCategory())
        },
        addProduct(input){
            dispatch(addProduct(input))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateProductForm)