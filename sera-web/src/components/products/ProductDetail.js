import React, {Component} from 'react'
import {connect} from 'react-redux'
import UserComments from '../comments/UserComments'
import {changeSearchTerm} from "../../actions/SearchAction"
import {withRouter} from "react-router-dom";
import {inflateProductName} from "../../utils/utils";
import ProductMiniDetails from "./ProductMiniDetails";
import {onSelectProduct, clearViewingProduct} from "../../actions/ProductAction";
import ProductReview from "./ProductReview";
import ReviewMenu from "./ReviewMenu";

class ProductDetail extends Component {
    state = {activeTab: 'review'}

    componentWillMount() {
        this.props.onSelectProduct(this.parseProductId(this.props.match.params.prodName))
    }

    componentWillUnmount() {
        this.props.clearViewingProduct()
    }

    async componentWillReceiveProps(nextProps) {
        const {product} = nextProps
        if (!product) return

        // Update search bar to display proper name
        nextProps.changeSearchTerm(nextProps.product.name)

        const productId = this.parseProductId(nextProps.match.params.prodName)
        if (productId !== this.parseProductId(this.props.match.params.prodName)) {
            nextProps.onSelectProduct(productId)
        }
    }

    componentDidUpdate(prevProps) {
        // Redirect if product not found (false value)
        if (this.props.product === false) {
            this.props.history.replace(`/product_not_found`)
            return
        }

        // Check url
        const urlProductName = this.parseProductName(this.props.match.params.prodName)
        const {product} = this.props
        const productNameInflated = inflateProductName(product.name)
        if (urlProductName !== productNameInflated) {
            this.props.history.replace(`${productNameInflated}-${product.productId}`)
        }
    }

    render() {
        const {product} = this.props
        if (!product) return (
            <div align='center' style={{paddingTop: 20}}>
                <div className="preloader-wrapper big active">
                    <div className="spinner-layer spinner-blue-only">
                        <div className="circle-clipper left">
                            <div className="circle"/>
                        </div><div className="gap-patch">
                        <div className="circle"/>
                    </div><div className="circle-clipper right">
                        <div className="circle"/>
                    </div>
                    </div>
                </div>
            </div>
        )

        return (
            <div className='row' style={{marginTop: 50}}>
                <div className='col s12 m3'>
                    <ProductMiniDetails product={this.props.product}/>
                    <button className='btn-flat green white-text' onClick={() => document.getElementById('comments').scrollIntoView({behavior: 'smooth'}) }>Comments</button>
                </div>
                <div className='offset-m3 col s12 m7' style={{position: 'absolute'}}>
                    <ProductReview className='col s12 m9' productId={product.productId}/>
                    <div className="col hide-on-small-only m9 sl2">
                        <UserComments
                            id='comments'
                            ref='comments'
                            productId={product.productId}
                        />
                    </div>

                </div>
                <ReviewMenu/>
            </div>
        )
    }

    parseProductName(prodName) {
        return prodName.split('-')[0]
    }

    parseProductId(prodName) {
        return prodName.split('-')[1]
    }
}

function mapStateToProps(state) {
    return {
        product: state.product.currentProduct
    }
}

export default connect(mapStateToProps, {changeSearchTerm, onSelectProduct, clearViewingProduct})(withRouter(ProductDetail))