import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import axios from 'axios'
import * as Apis from '../../actions/apis'
import {allProduct, allProducer, allParameterCategory, allProductCategory, allReviewCategory, addProduct, getProduct, getProductParams, getProductReview, editProduct} from "../../actions/index";
import {TextField, FormControl, Select, MenuItem, Input, InputLabel, Tab, Tabs, AppBar} from 'material-ui';
import SweetAlert from 'sweetalert-react'; // eslint-disable-line import/no-extraneous-dependencies
import 'sweetalert/dist/sweetalert.css';


function TabContainer(props) {
    return <div style={{ padding: 8 * 3 }}>{props.children}</div>;
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

class EditProductForm extends Component {

    state = {
        product_id: 0,
        name: "fdsfadsf",
        product_category: "",
        image: "",
        producer: "",
        producerId: 0,
        description: "",
        parameter: {},
        review: {},
        value: 0,
        returnStatus: '',
        returnMessage: ''
    };

    addParameter(parameterCategory, content) {
        this.state.parameter[parameterCategory] = content
    }

    addReview(reviewCategory, content) {
        this.state.review[reviewCategory] = content
    }

    componentDidMount() {
        const {id} = this.props.match.params
        this.props.allProducer()
        this.props.allParameterCategory()
        this.props.allReviewCategory()
        this.props.allProductCategory()

        if (id) {
            this.props.getProductReview(id)
            this.props.getProductParams(id)

            axios.get(Apis.GET_GET_PRODUCT, {
                params: {id: id}
            })
                .then(({data}) => {
                    console.log(data)
                    console.log(this.props.params)
                    console.log(this.props.review)
                    console.log(this.props.producers)
                    this.props.params.map(param => {
                        this.addParameter(param.parameterCategory.name, param.content)
                    })
                    this.props.review.map(r => {
                        this.addReview(r.reviewCategory.name, r.content)
                    })
                    this.setState({
                        product_id: id,
                        name: data.name,
                        product_category: data.product_category,
                        image: data.image,
                        producer: data.producer,
                        producerId: data.producer_id,
                        description: data.description,
                        value: 0
                    })

                })
                .catch(err => console.error(err))
        }

    }

    onFormSubmit(event) {
        event.preventDefault()
        this.setState({ show: true })
    }

    handleEditItem(){
        this.props.editProduct(this.state)
        this.setState({showSuccess: true})
        setTimeout(() => {
            this.setState({showSuccess: false})
            this.props.history.push('/')
        }, 2000)
    }

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const {classes} = this.props;
        const { value } = this.state;

        return(
            <div className="row">
                <form id="createProduct" onSubmit={this.onFormSubmit.bind(this)}>
                    <AppBar position="static" style={{backgroundColor: '#eee', color: '#000'}}>
                        <Tabs style={{textAlign: 'center'}} value={value} onChange={this.handleChange}>
                            <Tab label="Introduction" />
                            <Tab label="Parameter" />
                            <Tab label="Review" />
                        </Tabs>
                    </AppBar>
                    <div className="tab-content">
                        {value === 0 && <TabContainer>
                            <div className="row">
                                <div className="col s12 m6">
                                    <div className="form-group">
                                        <TextField
                                            id="name"
                                            label="Product Name"
                                            margin="normal"
                                            multiline
                                            fullWidth
                                            onChange={event => this.setState({name: event.target.value})}
                                            value={this.state.name}
                                            placeholder="Product Name"
                                        />
                                    </div>
                                </div>

                                <div className="col s12 m6">
                                    <div className="form-group">
                                        <TextField
                                            label="Image"
                                            margin="normal"
                                            multiline
                                            fullWidth
                                            onChange={event => this.setState({image: event.target.value})}
                                            name="image"
                                            id="image"
                                            value={this.state.image}
                                            placeholder="Image Link"
                                        />

                                    </div>
                                </div>

                                <div className="col s12 m6">
                                    <div className="form-group">
                                        <FormControl fullWidth>
                                            <InputLabel htmlFor="age-simple">Producer Name</InputLabel>
                                            <Select
                                                fullWidth
                                                value={this.state.producer}
                                                onChange={event => this.setState({producer: event.target.value})}
                                                input={<Input name="producer" id="producer" />}
                                            >
                                                <MenuItem value="">
                                                    <em>Producer Name</em>
                                                </MenuItem>
                                                {this.props.producers.map(producer => {
                                                    return (
                                                        <MenuItem
                                                            selected={producer.producerId==this.state.producerId}
                                                            key={producer.producerId}
                                                            value={producer.name}>{producer.name}</MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        </FormControl>
                                    </div>
                                </div>

                                <div className="col s12 m6">
                                    <div className="form-group">
                                        <FormControl fullWidth>
                                            <InputLabel htmlFor="age-simple">Product Category</InputLabel>
                                            <Select
                                                value={this.state.product_category}
                                                onChange={event => this.setState({product_category: event.target.value})}
                                                input={<Input name="product_category" id="product_category" />}
                                            >
                                                <MenuItem value="">
                                                    <em>Producer Category</em>
                                                </MenuItem>
                                                {this.props.productCategories.map(productCategory => {
                                                    return (
                                                        <MenuItem
                                                            selected={productCategory.name==this.state.product_category}
                                                            key={productCategory.product_category_id}
                                                            value={productCategory.name}>{productCategory.name}</MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        </FormControl>
                                    </div>
                                </div>


                                <div className="col m12">
                                    <div className="form-group">
                                        <TextField
                                            label="Description"
                                            margin="normal"
                                            multiline
                                            fullWidth
                                            onChange={event => this.setState({description: event.target.value})}
                                            name="description"
                                            id="description"
                                            value={this.state.description}
                                            placeholder="Description"
                                        />

                                    </div>
                                </div>
                            </div>
                        </TabContainer>}
                        {value === 1 && <TabContainer>
                            <div className="row">
                                {this.props.parameterCategories.map(parameterCategory => {
                                    let index = this.props.params.findIndex(x => x.parameterCategoryId == parameterCategory.parameter_category_id)
                                    let paramsContent = null;
                                    if(index != -1) {
                                        paramsContent = this.props.params[index].content;
                                    }

                                    return (
                                        <div className="col s12 m4">
                                            <div className="form-group">
                                                <TextField
                                                    label={parameterCategory.name}
                                                    multiline
                                                    fullWidth
                                                    margin="normal"
                                                    onChange={event => this.addParameter(parameterCategory.name, event.target.value)}
                                                    name={"parameterCategory_"+parameterCategory.parameter_category_id}
                                                    id={"parameterCategory_"+parameterCategory.parameter_category_id}
                                                    placeholder={parameterCategory.name}
                                                    defaultValue={this.state.parameter[parameterCategory.name]}
                                                />
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        </TabContainer>}
                        {value === 2 && <TabContainer>
                            <div className="row">
                                {this.props.reviewCategories.map(reviewCategory => {
                                    let index = this.props.review.findIndex(x => x.reviewCategoryId == reviewCategory.review_category_id)
                                    let reviewContent = null;
                                    if(index != -1) {
                                        reviewContent = this.props.review[index].content;
                                    }

                                    return (
                                        <div className="col s12 m6">
                                            <div className="form-group">
                                                <TextField
                                                    label={reviewCategory.name}
                                                    multiline
                                                    fullWidth
                                                    margin="normal"
                                                    onChange={event => this.addReview(reviewCategory.name, event.target.value)}
                                                    name={"reviewCategory_"+reviewCategory.review_category_id}
                                                    id={"reviewCategory_"+reviewCategory.review_category_id}
                                                    placeholder={reviewCategory.name}
                                                    defaultValue={this.state.review[reviewCategory.name]}
                                                />
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        </TabContainer>}
                    </div>
                    <button style={{display: 'flex', justifyContent: 'center', textAlign: 'center', margin: "0 auto"}} type="submit" className="btn btn-default" form="createProduct">Submit</button>
                    <SweetAlert
                        show={this.state.show}
                        title="Edit product"
                        text={"Are you want to Edit "+this.state.name}
                        type="warning"
                        showCancelButton
                        onConfirm={() => {
                            this.handleEditItem()
                        }}
                        onCancel={() => this.setState({ show: false })}
                    />
                    <SweetAlert
                        show={this.state.showSuccess}
                        title="Success"
                        text={"Edited " + this.state.name + " success"}
                        type='success'
                    />
                </form>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        producers: state.product.producers,
        reviewCategories: state.product.reviewCategories,
        productCategories: state.product.productCategories,
        parameterCategories: state.product.parameterCategories,
        product: state.product.product,
        params: state.product.params,
        review: state.product.review
    }
}

const mapDispatchToProps = dispatch => {
    return {
        allProduct(){
            dispatch(allProduct())
        },
        allProducer(){
            dispatch(allProducer())
        },
        allReviewCategory (){
            dispatch(allReviewCategory())
        },
        allProductCategory(){
            dispatch(allProductCategory())
        },
        allParameterCategory(){
            dispatch(allParameterCategory())
        },
        addProduct(input){
            dispatch(addProduct(input))
        },
        editProduct(input){
            dispatch(editProduct(input))
        },
        getProduct(id){
            dispatch(getProduct(id))
        },
        getProductParams(productId) {
            dispatch(getProductParams(productId))
        },
        getProductReview(productId) {
            dispatch(getProductReview(productId))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProductForm)