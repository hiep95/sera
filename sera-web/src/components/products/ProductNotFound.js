import React from 'react'

export default function ProductNotFound() {
    return <h4 style={{textAlign: 'center'}}>404: Product Not Found!</h4>
}
