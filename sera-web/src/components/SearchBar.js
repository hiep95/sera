import React, {Component} from 'react'
import {connect} from 'react-redux'
import {changeSearchTerm} from "../actions/SearchAction"
import {withRouter} from 'react-router-dom'

class SearchBar extends Component {
    onFormSubmit(event) {
        event.preventDefault()
        this.props.history.push(`/search/${this.props.term}`)
    }

    render() {
        return (
            <form
                className='input-group'
                onSubmit={this.onFormSubmit.bind(this)}
            >
                <div style={{display: 'flex', flexDirection: 'row'}} >
                    <input
                        style={styles.inputStyles}
                        onChange={event => this.props.changeSearchTerm(event.target.value)}
                        placeholder='Find product...'
                        value={this.props.term}
                    />
                    <span className='input-group-btn' >
                </span>
                </div>
            </form>
        )
    }
}

const styles = {
    inputStyles: {
        flex: 1,
        margin: 10,
    },
    buttonStyles: {
        flex: 0
    }
}

function mapStateToProps(state) {
    return {
        term: state.search.term
    }
}

export default connect(mapStateToProps, {changeSearchTerm})(withRouter(SearchBar))