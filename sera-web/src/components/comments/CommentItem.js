import React, {Component} from 'react'
import {connect} from 'react-redux'
import StarBar from "../common/StarBar";

class ReviewItem extends Component {
    render() {
        const {comment} = this.props
        if (!comment.user) comment.user = this.props.currentUser
        const {user} = comment

        return (
            <div style={{display: 'flex', flexDirection: 'row'}} >
                <div style={{width: '20%', position: 'relative'}}>
                    <img
                        src={user.avatar || '/imgs/noavatar.png'}
                        alt="Avatar"
                        className="circle responsive-img"
                        style={{
                            background: '#e0f2f1',
                            position: 'absolute',
                            objectFit: 'contain',
                            height: 'auto',
                            maxHeight: '50',
                            width: 60,
                            marginTop: 15,
                            right: 30,
                            alignSelf: 'right'
                        }}
                    />
                </div>
                <div style={{width: '80%', marginTop: 5, marginBottom: 5}}>
                    <div>
                        <div style={{padding: 10}}>
                            <div style={{display: 'flex'}}>
                                <span style={
                                    {
                                        fontStyle: 'light', fontStretch: 'semi-condensed', fontWeight: 400, fontSize: 20
                                    }
                                }>
                                    {user.name!='anonymus'?user.name:comment.username}
                                </span>
                                <StarBar style={{alignSelf: 'center', height: 12, marginLeft: 20}}
                                         score={comment.rating}
                                />
                                <label style={{marginLeft: 30, marginTop: 5}}>{
                                    comment.sourceId === 1 ? 'Tinhte.vn' : 'VNReview'
                                }</label>
                            </div>
                            <blockquote>
                                {comment.content || '(No comment)'}
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.auth.user
    }
}

export default connect(mapStateToProps)(ReviewItem)