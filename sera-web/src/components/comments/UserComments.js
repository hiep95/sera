import React, {Component} from 'react'
import {connect} from 'react-redux'
import {fetchComments} from "../../actions/CommentAction"
import CommentItem from './CommentItem'
import {postComment, onCommentTextChange} from "../../actions/CommentAction";
import {Input} from 'react-materialize'
import StarBar from "../common/StarBar";

class UserReview extends Component {
    state = {postingComment: false}

    componentWillMount() {
        this.props.fetchComments(this.props.productId)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.productId !== this.props.productId) {
            this.props.fetchComments(nextProps.productId)
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    onPostComment() {
        this.props.postComment(this.props.productId, this.props.comment, this.refs.rating.clickedScore)
        this.refs.rating.reset()
    }

    renderCommentInput() {
        if (!this.props.user) return <h6 style={{marginLeft: 60}}>To comment, you must login first!</h6>
        return (
            <div style={{display: 'flex', flexDirection: 'row'}}>
                <div style={{width: '20%', position: 'relative'}}>
                    <img
                        src={this.props.user.avatar || '/imgs/noavatar.png'}
                        alt="Avatar"
                        className="circle responsive-img"
                        style={{
                            background: '#e0f2f1',
                            position: 'absolute',
                            objectFit: 'contain',
                            height: 'auto',
                            maxHeight: '50',
                            width: 60,
                            marginTop: 30,
                            right: 30,
                            alignSelf: 'right'
                        }}
                    />
                </div>
                <div style={{width: '80%', display: 'flex', flexDirection: 'column', marginBottom: 30}}>
                    <div className="row">
                        <form className="col s12" style={{marginBottom: -50}}>
                            <div className="row">
                                <Input
                                    onChange={e => this.props.onCommentTextChange(e.target.value)}
                                    value={this.props.comment}
                                    placeholder='Comment something...'
                                    type='textarea'
                                    style={{width: '100%'}}
                                    s={12}
                                />
                            </div>
                        </form>
                    </div>
                    <div style={{flex: 1, position: 'relative', flexDirection: 'row'}}>
                        <div style={{position: 'absolute', right: 0}}>
                            <StarBar ref='rating' readOnly={false} style={{height: 15, display: 'inline-block', marginRight: 20}}/>
                            <button className='waves-effect waves-light btn' onClick={this.onPostComment.bind(this)}>Comment</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderCommentList() {
        const {comments} = this.props
        if (comments) {
            if (comments.length === 0) return <h4 style={{textAlign: 'center'}}>No comments yet!</h4>
            else return comments.map((comment, index) => {
                let divider = null
                if (index !== comments.length - 1) divider = <div style={{width: '100%', height: 1, marginLeft: 50, backgroundColor: '#c5cae9'}}/>
                return (
                    <div>
                        <CommentItem key={index} comment={comment} />
                        {divider}
                    </div>
                )
            })
        } else {
            return (
                <div className="progress">
                    <div className="indeterminate"/>
                </div>
            )
        }
    }

    render() {
        const {ref, id} = this.props
        return (
            <div ref={ref} id={id}>
                {this.renderCommentInput()}
                {this.renderCommentList()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        comments: state.comments.comments,
        comment: state.comments.commentText,
        user: state.auth.user
    }
}

export default connect(mapStateToProps, {fetchComments, postComment, onCommentTextChange})(UserReview)