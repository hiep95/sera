import React, {Component} from 'react'
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {crawl} from "../../actions/index";
import {Input, Row} from 'react-materialize';
import {TextField, FormControl, Select, MenuItem, InputLabel, Tab, Tabs, AppBar} from 'material-ui';
import axios from 'axios'

function TabContainer(props) {
    return <div style={{ padding: 8 * 3 }}>{props.children}</div>;
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

class Crawler extends Component {

    handleChange = (event, value) => {
        this.setState({ value });
    };

    state = {
        products: '',
        crawlResults: null,
        crawling: false,
        value: 0
    };

    componentDidMount() {
    }

    async onFormSubmit(event) {
        event.preventDefault()
        // const data = await axios.get("/crawl/iphone6_all")
        // console.log(data)
        this.props.crawl(this.state)
        this.setState({crawling:true})
    }

    handleAutoCrawl() {
        let productss = ['HTC 10', 'HTC One A9',
            'htc one m8',
            'HTC One M9',
            'HTC ONE E8',
            'HTC ONE MAX',
            'HTC U11',
            'LG G3',
            'LG G Pro 2',
            'Oppo A71',
            'Xiaomi A1',
            'Oppo F3 lite',
            'Sony Xeperia A1',
            'Sony Xperia Xz',
            'Oppo A39',
            'Xiaomi Redmi note 4',
            'Sony Xperia XA',
            'Oppo F1 plus',
            'Xiaomi Mi max 2',
            'Xiaomi Mi 4',
            'Xiaomi Mi 5',
            'Xiaomi mi 6',
            'iphone 6',
            'iphone 5',
            'samsung galaxy S7',
            'samsung galaxy s8',
            'samsung galaxy note 7',
            'samsung galaxy A7 2017',
            'samsung galaxy J7',
            'samsung galaxy A3',
            'asus zenphone 4',
            'Samsung Galaxy S6',
            'Samsung Galaxy E5',
            'Sam Sung Galaxy Note 3 Neo',
            'Samsung Galaxy S5',
            'HTC Desire 628'];
        // var processItems = function(x){
        //     if( x < productss.length ) {
        //         let s = {
        //             products: x
        //         };
        //
        //         this.props.crawl(s, function(res) {
        //             // add some code here to process the response
        //
        //             processItems(x+1);
        //         });
        //     }
        // };
        // processItems(0);
        // productss.map(p => {
        //     this.setState({products: p})
        //     this.props.crawl(this.state)
        //     this.setState({crawling:true})
        // })
    }

    _renderResults() {
        if (this.props.crawlResults) {
            console.log(this.props.crawlResults)
            this.setState({crawling:false})
                return (
                <div class="row">
                    <div class="col s12 m12">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">{this.props.crawlResults.Name}</span>
                                <p>{this.props.crawlResults.source}</p>
                                <p>{this.props.crawlResults.journal.substring(0, 400)+'...'}</p>
                            </div>
                        </div>
                    </div>
                </div>
                )
        } else if (this.state.crawling) {
            return <h4 style={{textAlign: 'center'}}>Running</h4>
        } else {
            return <h4 style={{textAlign: 'center'}}></h4>
        }
    }

    render() {
        const { value } = this.state;
        return(
        <div>
            <AppBar position="static" style={{backgroundColor: '#eee', color: '#000'}}>
                <Tabs style={{textAlign: 'center'}} value={value} onChange={this.handleChange}>
                    <Tab label="Crawler" />
                    <Tab label="Auto Crawler" />
                </Tabs>
            </AppBar>
            <div className="tab-content">
                {value === 0 && <TabContainer>
                <Row>
                    <div className="col s3"></div>
                    <div className="col s6">
                    <form id="createProduct" className="col s12"
                          onSubmit={this.onFormSubmit.bind(this)}>

                        <div className="row">
                            <Input
                                onChange={event => this.setState({products: event.target.value})}
                                name="name"
                                id="name"
                                type="text"
                                s={12} label="Product Name" />
                        </div>

                    </form>
                        <button style={{display: 'flex', justifyContent: 'center', textAlign: 'center', margin: "0 auto"}} type="submit" className="btn btn-default" form="createProduct">Submit</button>

                    </div>
                    <div className="col s3"></div>
                    <div className='row'>
                        {this._renderResults()}
                    </div>
                </Row>
                </TabContainer>}
                {value === 1 && <TabContainer>
                    <Row>
                        <div className="col s3"></div>
                        <div className="col s6">
                            <button onClick={this.handleAutoCrawl()}
                                style={{display: 'flex', justifyContent: 'center', textAlign: 'center', margin: "0 auto"}} type="submit" className="btn btn-default" form="createProduct">Auto Crawl</button>

                        </div>
                        <div className="col s3"></div>
                        <div className='row'>
                            {this._renderResults()}
                        </div>
                    </Row>
                </TabContainer>}
            </div>
        </div>
        );
    }
}

const mapStateToProps = state => {
    return {crawlResults: state.admin.crawlResults}
}

const mapDispatchToProps = dispatch => {
    return {
        crawl(input) {
            dispatch(crawl(input))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Crawler)