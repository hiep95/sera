import React, {Component} from 'react'
import {connect} from 'react-redux';
import {allProduct, allProducer, allParameterCategory, allProductCategory, allReviewCategory, addProduct, getNumberOfComment} from "../../actions/index";
import ReactHighcharts from 'react-highcharts';

class Analytic extends Component {

    state = {
        number_of_comment: null
    };

    config = {
        title: {
            text: 'User Access'
        },

        yAxis: {
            title: {
                text: 'Number of Access'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        series: [{
            data: [29, 71, 106, 129, 144, 176, 135, 148, 216, 194, 295, 454]
        }]
    };

    config2 = {
        title: {
            text: 'User Comment'
        },

        yAxis: {
            title: {
                text: 'Number of Comment'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        series: [{
            data: [12, 45, 67, 43, 13, 34, 45, 54, 12, 17, 26, 34]
        }]
    };

    addParameter(parameterCategory, content) {
        this.state.parameter[parameterCategory] = content
    }

    addReview(reviewCategory, content) {
        this.state.review[reviewCategory] = content
    }

    componentDidMount() {
        getNumberOfComment();
    }

    render() {
        return(
            <div className="row" style={{marginTop: 50}}>
                <ReactHighcharts config={this.config} ref="chart"></ReactHighcharts>
                <ReactHighcharts config={this.config2} ref="chart"></ReactHighcharts>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        producers: state.product.producers,
        reviewCategories: state.product.reviewCategories,
        productCategories: state.product.productCategories,
        parameterCategories: state.product.parameterCategories
    }
}

const mapDispatchToProps = dispatch => {
    return {
        allProduct(){
            dispatch(allProduct())
        },
        allProducer(){
            dispatch(allProducer())
        },
        allReviewCategory (){
            dispatch(allReviewCategory())
        },
        allProductCategory(){
            dispatch(allProductCategory())
        },
        allParameterCategory(){
            dispatch(allParameterCategory())
        },
        addProduct(input){
            dispatch(addProduct(input))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Analytic)