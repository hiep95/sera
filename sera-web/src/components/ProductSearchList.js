import React, {Component} from 'react'
import {connect} from 'react-redux'
import ProductItem from './products/ProductItem'
import {searchProduct, changeSearchTerm, searchProductsByProducerName} from "../actions/SearchAction"
import SuggestedPostList from "./SuggestedPostList";

class ProductSearchList extends Component {
    componentWillMount() {
        const {term, type} = this.props.match.params
        this.updateSearch(term, type)
    }

    componentWillReceiveProps(nextProps) {
        const {term, type} = nextProps.match.params
        if (term !== this.props.match.params.term || type !== this.props.match.params.type) {
            this.updateSearch(term, type)
        }
    }

    updateSearch(term, type) {
        if (type === 'producer') this.props.searchProductsByProducerName(term)
        else this.props.searchProduct(term)
        this.props.changeSearchTerm(term)
    }

    _renderList() {
        if (this.props.products.length > 0) {
            return this.props.products.map(product => {
                return (
                    <ProductItem product={product} key={product.product_id}/>
                )
            })
        } else {
            return <h4 style={{textAlign: 'center'}}>No product found!</h4>
        }
    }

    render() {
        return (
            <div style={{marginTop: 50}}>
                <div className='row'>
                    <div className="col s12 m1">

                    </div>
                    <div className="col s12 m10">
                        {this._renderList()}
                    </div>
                    <div className="col s12 m1">
                        <SuggestedPostList/>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {products: state.search.products}
}

export default connect(mapStateToProps, {searchProduct, changeSearchTerm, searchProductsByProducerName})(ProductSearchList)