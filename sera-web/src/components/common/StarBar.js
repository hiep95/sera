import React, {Component} from 'react'

class StarBar extends Component {
    state = {hoverOn: -1}
    readOnly = (this.props.readOnly === undefined) ? true : (this.props.readOnly)
    clickedScore = -1

    onMouseOverHandler(index) {
        if (this.readOnly) return
        this.setState({hoverOn: index + 1})
    }

    onMouseClickHandler(index) {
        if (this.readOnly) return
        this.clickedScore = index + 1
    }

    reset() {
        this.clickedScore = -1
        this.setState({hoverOn: -1})
    }

    renderStar(score) {
        const result = []
        let max
        if (this.state.hoverOn > 0) max = this.state.hoverOn
        else if (this.clickedScore >= 0) max = this.clickedScore
        else max = Math.floor(score || 0)

        for (let i = 0; i < 10; i++) {
            const src = (i < max) ? require('../../images/star_full.png') : require('../../images/star_empty.png')
            result.push(
                <img
                    alt='rating'
                    key={i}
                    src={src}
                    style={{height: '100%', width: 'auto', marginLeft: 1, marginRight: 1}}
                    onMouseOver={this.onMouseOverHandler.bind(this, i)}
                    onMouseOut={() => this.setState({hoverOn: -1})}
                    onClick={this.onMouseClickHandler.bind(this, i)}
                />
            )
        }
        return result
    }

    render() {
        const {score} = this.props

        return (
            <div style={{display: 'flex', flexDirection: 'row', ...this.props.style}}>
                {this.renderStar(score)}
            </div>
        )
    }
}

export default StarBar