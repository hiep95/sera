import React, {Component} from 'react'
import SearchBar from './SearchBar'
import {withRouter} from "react-router-dom";
import axios from 'axios'
import {GET_TOP_PRODUCTS} from "../actions/apis";
import ProductItem from "./products/ProductItem";
import SuggestedPostList from "./SuggestedPostList";
import ProductSearchList from "./ProductSearchList";


class Home extends Component {
    state = {topProducts: []}

    async componentDidMount() {
        const {data} = await axios.get(GET_TOP_PRODUCTS)
        this.setState({topProducts: data})
    }

    _renderTopProducts() {
        return this.state.topProducts.map(product => {
            return <ProductItem product={product} key={product.productId} />
        })
    }

    render() {
        return (
            <div style={{marginTop: 50}}>
                <div className='row'>
                    <div className="col s12 m1">

                    </div>
                    <div className="col s12 m10">
                        {this._renderTopProducts()}
                    </div>
                    <div className="col s12 m1">
                        <SuggestedPostList/>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Home)