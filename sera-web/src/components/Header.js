import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link, withRouter} from 'react-router-dom'
import {Drawer, MenuItem, Divider} from "material-ui";
import {GET_ALL_PRODUCERS} from "../actions/apis";
import axios from 'axios'
import SearchBar from './SearchBar';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import List from 'material-ui/List';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';

import {withStyles} from 'material-ui/styles';


const drawerWidth = 240;

const styles = theme => ({
    paper: {
        height: 'calc(100% - 64px)',
        top: 64,
        backgroundColor: '#007b6e',
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
    },
    root: {
        width: '100%',
        height: 430,
        marginTop: theme.spacing.unit * 3,
        zIndex: 1,
        overflow: 'hidden',
    },
    appFrame: {
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100%',
    },
    appBar: {
        position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'appBarShift-left': {
        marginLeft: drawerWidth,
    },
    'appBarShift-right': {
        marginRight: drawerWidth,
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        marginTop: 64,
        position: 'relative',
        height: '100%',
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        width: '100%',
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        height: 'calc(100% - 56px)',
        marginTop: 56,
        [theme.breakpoints.up('sm')]: {
            content: {
                height: 'calc(100% - 64px)',
                marginTop: 64,
            },
        },
    },
    'content-left': {
        marginLeft: -drawerWidth,
    },
    'content-right': {
        marginRight: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShift-left': {
        marginLeft: 0,
    },
    'contentShift-right': {
        marginRight: 0,
    },
});

class Header extends Component {
    state = {open: false, producers: null}

    handleDrawerOpen = () => {
        if(this.state.open == false){
            this.setState({ open: true });
        }
        else{
            this.setState({ open: false });
        }
    };

    handleDrawerClose = () => {
        this.setState({ open: false });
    };

    handleChangeAnchor = event => {
        this.setState({
            anchor: event.target.value,
        });
    };

    async componentDidMount() {
        try {
            const {data} = await axios.get(GET_ALL_PRODUCERS)
            this.setState({producers: data})
        } catch (err) {
            this.setState({producers: false})
        }
    }

    _renderRightElements() {
        const {user} = this.props

        if (user === false) { // Not logged
            return ([
                <li><a key='1' href='/auth/google'>Login Google</a></li>,
                <li><a key='2' href='/auth/facebook'>Login Facebook</a></li>
            ])
        } else if (user) {
            return (
                [
                    <li key='1' style={{marginRight: 15}}>Welcome, {user.name}</li>,
                    <li key='2'><a href='/api/auth/logout'>Logout</a></li>,
                ]
            )
        } else { // Loading

        }
    }

    _renderMenuHeader() {
        const {user} = this.props

        if (user == false || !user || user == null) { // Not logged
            return (
                <MenuItem style={{height: 70, margin: 0}}>
                    <div className="row" style={{height: 50, margin: 0, padding: 10}}>
                        <div className="col s4 m4">
                            <img style={{width: 50, padding: 0}} src="https://t3.ftcdn.net/jpg/01/04/10/10/240_F_104101070_wbEDt3CmlzqnPbdmOlVCL7Q7yu9mCduz.jpg" alt="" className="circle responsive-img valign profile-image"/>
                        </div>
                        <div className="col s8 m8">
                            <a className="btn-flat waves-effect waves-light white-text profile-btn" href="#" style={{padding: 0}} data-activates="profile-dropdown"></a>
                            <p className="user-roal">Guest</p>
                        </div>
                    </div>
                </MenuItem>

            )
        } else if (user && user.role == 'admin') {
            return (
                <MenuItem style={{height: 70, margin: 0}}>
                        <div className="row" style={{height: 50, margin: 0, padding: 10}}>
                            <div className="col col s4 m4 l4">
                                <img src={user.avatar} alt="" className="circle responsive-img valign profile-image"/>
                            </div>
                            <div className="col col s8 m8 l8">
                                <a className="btn-flat waves-effect waves-light white-text profile-btn" href="#" style={{padding: 0}} data-activates="profile-dropdown">{user.name}</a>
                                <p className="user-roal">Administrator</p>
                            </div>
                        </div>
                </MenuItem>

            )
        } else { // Loading
            return (
                <MenuItem style={{height: 70, margin: 0}}>
                    <div className="row" style={{height: 50, margin: 0, padding: 10}}>
                        <div className="col col s4 m4 l4">
                            <img src={user.avatar} alt="" className="circle responsive-img valign profile-image"/>
                        </div>
                        <div className="col col s8 m8 l8">
                            <a className="btn-flat waves-effect waves-light white-text profile-btn" href="#" style={{padding: 0}} data-activates="profile-dropdown">{user.name}</a>
                            <p className="user-roal">User</p>
                        </div>
                    </div>
                </MenuItem>

            )
        }
    }

    _renderDrawerMenuItem() {
        const {producers} = this.state
        if (!producers) return
        else if (producers === false) return <MenuItem>(Có lỗi, vui lòng F5)</MenuItem>
        else if (producers.length === 0) return <MenuItem>(Không có dữ liệu)</MenuItem>

        return producers.map(producer => {
            return <MenuItem key={producer.producerId} onClick={() => this.props.history.push(`/search/producer/${producer.name}`)}>
                {producer.name}
            </MenuItem>
        })
    }

    _renderMenuItem() {
        const {user} = this.props

        if (user === false) { // Not logged

        } else if (user && user.role == 'admin') {
            return (
                <div>
                    <MenuItem style={{color: 'white'}} onClick={() => this.props.history.push(`/admin/product/new`)}>
                        Add Product Review
                    </MenuItem>
                    <Divider/>
                    <MenuItem style={{color: 'white'}} onClick={() => this.props.history.push(`/admin/crawler`)}>
                        Crawler
                    </MenuItem>
                    <Divider/>
                    <MenuItem style={{color: 'white'}} onClick={() => this.props.history.push(`/admin/analytic`)}>
                        Analytic
                    </MenuItem>
                </div>
            )
        } else { // Loading

        }
    }

    render() {
        const { classes, theme } = this.props;
        const { anchor, open } = this.state;

        const drawer = (
            <Drawer
                type="persistent"
                classes={{
                    paper: classes.drawerPaper,
                }}
                anchor={anchor}
                open={open}
            >
                <div className={classes.drawerInner}>
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={this.handleDrawerClose}>
                            {theme.direction === 'rtl' ? 'ab' : 'ba'}
                        </IconButton>
                    </div>
                    <Divider />
                    <List className={classes.list}></List>
                    <Divider />
                    <List className={classes.list}></List>
                </div>
            </Drawer>
        );

        let before = null;
        let after = null;

        if (anchor === 'left') {
            before = drawer;
        } else {
            after = drawer;
        }

        return (
            <div>
                <div className="navbar-fixed scrollspy" id="#home">
                    <nav style={{backgroundColor: "#00897b"}}>
                        <div className='nav-wrapper container-fluid' >
                            <ul className='left'>
                                <li><a onClick={this.handleDrawerOpen}><i className="material-icons" >menu</i></a></li>
                                <li>
                                    <Link to={'/'} style={{marginLeft: 20, paddingLeft: 15, paddingRight: 15, fontSize: '2.1rem'}} className="brand-logo">
                                        Sera
                                    </Link>
                                </li>
                            </ul>

                            <div className="header-search-wrapper hide-on-med-and-down" >
                                <SearchBar/>
                            </div>

                            <ul className='right hide-on-med-and-down'>
                                {this._renderRightElements()}
                            </ul>
                        </div>

                    </nav>

                </div>
                <Drawer classes={{paper: classes.paper}} style={{width: 100}} type="persistent" containerStyle={{height: 'calc(100% - 64px)', top: 64, width: 100}} width='100' swipeAreaWidth='50' open={this.state.open} onRequestClose={() => this.setState({open: false})}>

                    <Divider/>
                    {this._renderMenuHeader()}
                    <Divider/>
                    {this._renderMenuItem()}

                </Drawer>


            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.auth.user
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default connect(mapStateToProps)(withRouter(withStyles(styles, { withTheme: true })(Header)))