import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link, withRouter} from 'react-router-dom'
import {Drawer, MenuItem, Divider} from "material-ui";
import {GET_ALL_PRODUCERS} from "../actions/apis";
import axios from 'axios'
import SearchBar from './SearchBar';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import List from 'material-ui/List';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';

import {withStyles} from 'material-ui/styles';


const drawerWidth = 240;

const styles = theme => ({
    paper: {
        height: 'calc(100% - 64px)',
        top: 64,
        backgroundColor: '#007b6e'
    },
    root: {
        width: '100%',
        height: 430,
        marginTop: theme.spacing.unit * 3,
        zIndex: 1,
        overflow: 'hidden',
    },
    appFrame: {
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100%',
    },
    appBar: {
        position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'appBarShift-left': {
        marginLeft: drawerWidth,
    },
    'appBarShift-right': {
        marginRight: drawerWidth,
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        marginTop: 64,
        position: 'relative',
        height: '100%',
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        width: '100%',
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        height: 'calc(100% - 56px)',
        marginTop: 56,
        [theme.breakpoints.up('sm')]: {
            content: {
                height: 'calc(100% - 64px)',
                marginTop: 64,
            },
        },
    },
    'content-left': {
        marginLeft: -drawerWidth,
    },
    'content-right': {
        marginRight: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShift-left': {
        marginLeft: 0,
    },
    'contentShift-right': {
        marginRight: 0,
    },
});

class SuggestedPostList extends Component {
    state = {open: false, producers: null}

    async componentDidMount() {
        try {
            const {data} = await axios.get(GET_ALL_PRODUCERS)
            this.setState({producers: data})
        } catch (err) {
            this.setState({producers: false})
        }
    }

    _renderDrawerMenuItem() {
        const {producers} = this.state
        if (!producers) return
        else if (producers === false) return <MenuItem>(Có lỗi, vui lòng F5)</MenuItem>
        else if (producers.length === 0) return <MenuItem>(Không có dữ liệu)</MenuItem>

        return producers.map(producer => {
            return <MenuItem key={producer.producerId} onClick={() => this.props.history.push(`/search/producer/${producer.name}`)}>
                {producer.name}
            </MenuItem>
        })
    }

    render() {

        return (
            <div>
                {this._renderDrawerMenuItem()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.auth.user
    }
}

SuggestedPostList.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default connect(mapStateToProps)(withRouter(withStyles(styles, { withTheme: true })(SuggestedPostList)))