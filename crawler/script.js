var PythonShell = require('python-shell');

var options = {
    mode: 'text',
    pythonOptions: ['-u'],
    args: ['iphone 6', 'iphone 7', 'oppo f1s']
};

var pyshell = new PythonShell("crawl_web_vnreview.py",options);

pyshell.on('message', function (message) {
    // received a message sent from the Python script (a simple "print" statement)
    console.log(message);
});

// end the input stream and allow the process to exit
pyshell.end(function (err) {
    if (err){
        throw err;
    };

    console.log('finished');
});